'use strict';
// Declare app level module which depends on filters, and services
angular.module('traingingym', ['app.controllers','ngRoute', 'ngCookies', 'ngAnimate'])

	//Quitar cosas cacheadas, SOLO EN DESARROLLO
	.config(['$httpProvider', function($httpProvider) {
		//initialize get if not there
		if (!$httpProvider.defaults.headers.get) {
			$httpProvider.defaults.headers.get = {};    
		}
		// Answer edited to include suggestions from comments
		// because previous version of code introduced browser-related errors

		//disable IE ajax request caching
		$httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
		// extra
		$httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
		$httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
		$httpProvider.defaults.headers.get['Content-Type'] = 'text/plain; charset=utf-8';
	}])

;













