//Añadir al generador de skins la clase o-n para darle opacidad a un elemento

'use strict';

/* Controllers */
angular.module('app.controllers', [])
	.controller('AppCtrl', function ($scope, $window, $cookies, $location, $http) {
		var self = this;

		//$http.get("http://localhost:5000/api/getToken").then(function(response){
			self.token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IlRpb0d5d3dsaHZkRmJYWjgxM1dwUGF5OUFsVSIsImtpZCI6IlRpb0d5d3dsaHZkRmJYWjgxM1dwUGF5OUFsVSJ9.eyJhdWQiOiJodHRwczovL2FuYWx5c2lzLndpbmRvd3MubmV0L3Bvd2VyYmkvYXBpIiwiaXNzIjoiaHR0cHM6Ly9zdHMud2luZG93cy5uZXQvNmFhMzQ5OWQtMzgzNS00MDJmLTgzYTUtNzE3Y2ZkMGJkOWMxLyIsImlhdCI6MTUzMTczODMzNSwibmJmIjoxNTMxNzM4MzM1LCJleHAiOjE1MzE3NDIyMzUsImFjciI6IjEiLCJhaW8iOiI0MkJnWURqOXlkTEFWR1JDVjNab2l1SnQ3L3RlK3dJTmR3bmZObnRrL0dSdUV2c2MrUllBIiwiYW1yIjpbInB3ZCJdLCJhcHBpZCI6IjM3MWZiZDE1LTU5YzMtNGY0Yi04OWI1LWU1M2RjYzJlMzAzYiIsImFwcGlkYWNyIjoiMSIsImZhbWlseV9uYW1lIjoiVklMQ0hFWiBGRVJOQU5ERVoiLCJnaXZlbl9uYW1lIjoiTSBBTkdFTEVTIiwiaXBhZGRyIjoiODguMTIuNTEuMTgxIiwibmFtZSI6IkdZRjM2NSwgU0wiLCJvaWQiOiJkODJlMmFmOS1hZDcwLTRiMGEtOWNlOC01NDNkNjliZmU3NWQiLCJwdWlkIjoiMTAwM0JGRkRBMjIyNjg5NCIsInNjcCI6IkNvbnRlbnQuQ3JlYXRlIERhc2hib2FyZC5SZWFkLkFsbCBEYXRhc2V0LlJlYWRXcml0ZS5BbGwgR3JvdXAuUmVhZCBSZXBvcnQuUmVhZFdyaXRlLkFsbCIsInN1YiI6IlE3U3U1VDRRRVBSLXdTSFpsQldsT180RXR6LUppNUY2WktnR0M0SDV1cVUiLCJ0aWQiOiI2YWEzNDk5ZC0zODM1LTQwMmYtODNhNS03MTdjZmQwYmQ5YzEiLCJ1bmlxdWVfbmFtZSI6ImFkbWluQHVuaXZlcnNpdHlwb3dlcmJpLm9ubWljcm9zb2Z0LmNvbSIsInVwbiI6ImFkbWluQHVuaXZlcnNpdHlwb3dlcmJpLm9ubWljcm9zb2Z0LmNvbSIsInV0aSI6Il9ndjBraV9reTBXU0JsMk5VYnNmQUEiLCJ2ZXIiOiIxLjAiLCJ3aWRzIjpbIjYyZTkwMzk0LTY5ZjUtNDIzNy05MTkwLTAxMjE3NzE0NWUxMCJdfQ.hDlMKIEpzXQnNqxPxtHTb9nWA3Tc0sj4FIL4XsGoGpwqGLLg8A57RDM7VlFbqdNkDxIQwAPRA-fW2Rqzlb__snO6GqNc564h8zej4z5VercwoTdBg1CUaPLxkL1ciJrmrzpv23qwIrE2lInRQoleH2qBlo6V3o1sYjJlF4B3XpgoeliNywbXaWy7T5SwAsIRTNOaywDFPw82EJi-XMRraBFagXo53UXS-Rss9QEVB6x4UeG-i5PkiNBhzBtrdEu0I59lf9pmlCCpcfCg6EAmBz9moLHuLzoIA9dSzizcgOaON2Pe1mDMgQHgi9KU1M3x-evlVecnBVH91AXCX0Cohg";
			self.dash = {link:"https://app.powerbi.com/reportEmbed?reportId=af2be5c9-9a73-4db7-b3ba-d978625ee8ad&groupId=aa920c86-8c0f-4ea8-9cd2-793b8cde1a7d"};
		//});
	})
	
	.directive('powerbiResource', function(){
	    return function (scope, element, attrs) {
	    	if(attrs.size && attrs.size == "auto"){
	    		var alto = $(element).height() < 320 ? 320 : $(element).height();
	    		var ancho = Math.round(alto * 1.618033988);
	    		element[0].parentNode.style.width = ancho+"px";
	    		element[0].style.width = ancho+"px";
	    		element[0].style.height = alto+"px";
	    	}else if(attrs.size && attrs.size == "square"){
	    		var ancho = $(element).width();
	    		element[0].style.height = ancho+"px";
	    	}

	        var obj = scope.$eval(attrs.powerbiResource);
	        var filters = [];
	        if(obj.filtros){
	        	for(var i = 0; i < obj.filtros.length; i++){
	        		var value = obj.filtros[i].tipo == 1 ? parseInt(obj.filtros[i].valor) : obj.filtros[i].valor.toString();
	    			var filter = {
						$schema: "http://powerbi.com/product/schema#basic",
						target: {
							table: obj.filtros[i].tabla,
							column: obj.filtros[i].columna
						},
						operator: "In",
						values: [value]
					}
					filters.push(filter);
	    		}
	        }
	        var token = scope.$eval(attrs.token);

	        var type = "";
	        if(obj.link.indexOf("dashboard") > 0) type = "dashboard";
			else if(obj.link.indexOf("report") > 0) type = "report";

			powerbi.reset(element[0]);
			powerbi.wpmp.stop();
			var models = window['powerbi-client'].models;
			var config = {
		        type: type,
		        accessToken: token,
		        embedUrl: obj.link,
		        filters:filters,
			    settings: {
					filterPaneEnabled: (typeof obj.filterPane != "undefined" ? obj.filterPane == 1 : true),
					navContentPaneEnabled: (typeof obj.navContentPane != "undefined" ? obj.navContentPane == 1 : true)
				}
		    };

		    // Embed the report and display it within the div container.
		    var report = powerbi.embed(element[0], config);
		    report.off("loaded");
		    // report.on will add an event handler which prints to Log window.
		    report.on("error", function (event) {
		    	console.log(event);
		    });
	    };
	})

	String.prototype.replaceAll = function(search, replacement) {
	    var target = this;
	    return target.split(search).join(replacement);
	};

	function formatNumber(val){
		if(typeof val != "string") val = val+"";

		if(val.indexOf(".") > -1) val = val.replace(".", ",");

		if(val.indexOf(",") > -1){
			var arrNumber = val.split(",");
		}

		var ret = typeof arrNumber != "undefined" ? arrNumber[0] : val;

		ret = ret.replace(".", "");
		var res = "";
		if(ret && ret.length > 3 && (ret.length == 3 && ret[0] != "-")){
			ret = ret.replace(".", "");
			var count = 1;
			for(var i = ret.length-1; i > -1; i--){
				if(count % 3 === 0 && i > 0) res = "."+ret[i]+res;
				else res = ret[i]+res;
				count++;
			}
		}else{
			res = ret;
		}
		if(typeof arrNumber != "undefined" && arrNumber[1] == "") arrNumber[1] = "0";
		return typeof arrNumber != "undefined" ? res+','+arrNumber[1] : res;
	}