angular.module('ngLoader', []);

/**
Declaramos la ruta relativa del módulo para saber donde ir a buscar las plantillas
**/
(function(){ 'use strict' //PATH
	var scripts = document.getElementsByTagName("script");
	angular.module('ngLoader').value("ngLoader.PATH", scripts[scripts.length-1].src.substring(0, scripts[scripts.length-1].src.lastIndexOf('/') + 1));
})();

(function(){ 'use strict'
	angular.module('ngLoader').directive("ngLoader", ["ngLoader.PATH", function(PATH){
		return {
			restrict:"AEC",
			templateUrl:function($element, $attrs){
				var theme = $attrs.theme;
				if ( !theme ) theme = "double-bounce";
				return PATH+"/"+theme+".html";
			},
			link:function($scope, $element, $attrs){
				if ( "size" in $attrs ){
					$element.css("width", $attrs.size+"px");
					$element.css("height", $attrs.size+"px");	
				}
			}
		}
	}]);
})();