/**
Los controles definidos mÃƒÂ¡s adelante necesitan hacer uso de los providers definidos en mi mÃƒÂ³dulo ngSearch.
Aunque lo normal es que ambos mÃƒÂ³dulos vayan juntos, no quiero forzar la dependencia de archivos, por lo que he incluido lo bÃƒÂ¡sico
del mÃƒÂ³dulo a continuaciÃƒÂ³n por si no estuviese declarado.
Es importante resaltar, no obstante, que si utilizamos el mÃƒÂ³dulo ngSearch, ÃƒÂ©ste tendrÃƒÂ¡ siempre prioridad sobre el que se define a continuaciÃƒÂ³n
**/

(function(){ 'use strict' //ngSearch
	try{
		var ngSearch = angular.module("ngControlsSearch");
	}
	catch (err){
		angular.module('ngControlsSearch', []);
		angular.module('ngControlsSearch').factory('ngControlsSearch', function(){
			var Latinise={};
			Latinise.latin_map={"ÃƒÂ":"A","Ãƒâ€š":"A","Ãƒâ€ž":"A","Ãƒâ‚¬":"A","Ãƒâ€¦":"A","ÃƒÆ’":"A","Ãƒâ€ ":"AE","Ãƒâ€¡":"C","ÃƒÂ":"D","Ãƒâ€°":"E","ÃƒÅ ":"E","Ãƒâ€¹":"E","ÃƒË†":"E","Ã†â€™":"F","ÃƒÂ":"I","ÃƒÅ½":"I","ÃƒÂ":"I","ÃƒÅ’":"I","Ãƒâ€˜":"N","Ãƒâ€œ":"O","Ãƒâ€":"O","Ãƒâ€“":"O","Ãƒâ€™":"O","ÃƒËœ":"O","Ãƒâ€¢":"O","Ã… ":"S","ÃƒÅ¡":"U","Ãƒâ€º":"U","ÃƒÅ“":"U","Ãƒâ„¢":"U","ÃƒÂ":"Y","Ã…Â¸":"Y","Ã…Â½":"Z","Ã…â€™":"OE","ÃƒÂ¡":"a","ÃƒÂ¢":"a","ÃƒÂ¤":"a","Ãƒ ":"a","ÃƒÂ¥":"a","ÃƒÂ£":"a","ÃƒÂ¦":"ae","ÃƒÂ§":"c","ÃƒÂ©":"e","ÃƒÂª":"e","ÃƒÂ«":"e","ÃƒÂ¨":"e","ÃƒÂ­":"i","ÃƒÂ®":"i","ÃƒÂ¯":"i","ÃƒÂ¬":"i","ÃƒÂ±":"n","ÃƒÂ³":"o","ÃƒÂ´":"o","ÃƒÂ¶":"o","ÃƒÂ²":"o","ÃƒÂ¸":"o","ÃƒÂµ":"o","Ã…Â¡":"s","ÃƒÂº":"u","ÃƒÂ»":"u","ÃƒÂ¼":"u","ÃƒÂ¹":"u","ÃƒÂ½":"y","ÃƒÂ¿":"y","Ã…Â¾":"z","Ã…â€œ":"oe"};

			return {
				parse_str:function(str){
					var arr = str.split("&");
					var obj = {};
					
					for ( var i = 0; i < arr.length; i++ ){
						var arr2 = arr[i].split("=");
						obj[arr2[0]] = arr2[1];
					}
					
					return obj;
				},
				removeAccents:function(string){return string.replace(/[^A-Za-z0-9\[\] ]/g,function(a){return Latinise.latin_map[a]||a})},
				isLatin:function(string){ return this.removeAccents(string) == string; },
				endsWith:function(string, termination, position){
					var subjectString = string.toString();
					if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
						position = subjectString.length;
					}
					position -= termination.length;
					var lastIndex = subjectString.indexOf(termination, position);
					return lastIndex !== -1 && lastIndex === position;
				},
				replaceTermination:function(string, $find, $replace){
					var $regexp = new RegExp($find+"$");
					return string.replace($regexp, $replace);
				},
				extractKeywords:function(string) {
					var $string = new String(string);
					$string = this.removeConnectors(this.removeAccents($string));
					$string = $string.replace(/[^A-Za-z0-9_]/g, '-');
					var $keywords = $string.split("-");
					for ( var i = 0; i< $keywords.length; i++ ) { 
						$keywords[i] = this.removePlurals($keywords[i]).toLowerCase();	
					}
					return $keywords;
				},
				removePlurals:function(string){
					var $string = string;
					
					if ( this.endsWith(string, "ones") ) $string = this.replaceTermination(string, "ones", "ÃƒÂ³n"); 
					else if ( this.endsWith(string, "tes") ) $string = this.replaceTermination(string, "tes", "te");
					else if ( this.endsWith(string, "es") ) $string = this.replaceTermination(string, "es", "");
					else if ( this.endsWith(string, "as") ) { $string = this.replaceTermination(string, "as", "a"); }
					else if ( this.endsWith(string, "s") && !this.endsWith(string, "us") ) $string = this.replaceTermination(string, "s", "");
					return $string;
				},
				removeConnectors:function(string) { 
					var $conectores = ["y", "de", "a", "del", "el", "la", "los", "las", "en", "entre", "desde", "hasta"];
					var $regexp = /\s/g; 
					var $words = string.split($regexp);

					var $valid_words =[];
					for ( var i in $words ){
						if ( $conectores.indexOf($words[i]) == -1 && $words[i].length > 2){
							$valid_words.push($words[i]);
						}
					}
					return $valid_words.join(" ");
				},
				levenshtein:function(a, b) {
					if(a.length == 0) return b.length; 
					if(b.length == 0) return a.length;

					// swap to save some memory O(min(a,b)) instead of O(a)
					if(a.length > b.length) {
						var tmp = a;
						a = b;
						b = tmp;
					}

					var row = [];
					// init the row
					for(var i = 0; i <= a.length; i++){
						row[i] = i;
					}

					// fill in the rest
					for( var i = 1; i <= b.length; i++){
						var prev = i;
						for(var j = 1; j <= a.length; j++){
							var val;
							if(b.charAt(i-1) == a.charAt(j-1)){
								val = row[j-1]; // match
							} else {
								val = Math.min(row[j-1] + 1, prev + 1, row[j] + 1);
							}
							row[j - 1] = prev;
							prev = val;
						}
						row[a.length] = prev;
					}

					return row[a.length];
				}
			}
		});
		angular.module('ngControlsSearch').factory('ngControlsSearch', function($q, $timeout, $parse, StringService){
			//Esta funciÃƒÂ³n rellena las opciones del buscador con valores predeterminados si no son provistos en la entrada de la funciÃƒÂ³n.
			function buildOptions(options){
				options = options || {};
				options.page = options.page || 1;
				options.limit = options.limit || 25;
				options.searchIn = options.searchIn || "*";
				if ( options.searchIn instanceof Array ) options.searchIn = options.searchIn.join(",");
				options.chunkSize = options.chunkSize || 100;
				options.sort = options.sort || undefined;
				
			}
			
			//Esta funciÃƒÂ³n va construyendo un mapa de pesos en funciÃƒÂ³n de la bÃƒÂºsqueda realizada
			function buildWeightMap(collection, wordsSearched, $index, weights, options){
				for ( var k = 0; k < options.chunkSize; k++ ){
					if ( $index >= collection.length ) break;

					var item = collection[$index];
					var weight = 0;	
					for ( var i in wordsSearched ){
						var word = StringService.removeAccents(wordsSearched[i]).toLowerCase();
						if ( word.length < 3 ) continue;
						var distance = 100;

						if ( typeof options.searchIn == "string" ){
							if ( options.searchIn == "*" ){
								for ( var key in item ){
									if ( !item[key] ) continue;
									var keywords = StringService.extractKeywords(item[key].toString());
									for ( var j in keywords ){
										if ( !keywords[j] ) continue;	
										
										var keyword = StringService.removeAccents(keywords[j].toString()).toLowerCase();
										
										distance = Math.min( StringService.levenshtein(keyword, word), distance );

										if ( keyword.indexOf(word) !== -1 ){
											distance = Math.min(distance, 0.5);
										}
									}
								}
							}
							else{
								var $searchInArray = options.searchIn.split(",").map(function(a){ return a.trim(); });
								for ( var ii = 0; ii < $searchInArray.length; ii++ ){
									var textoIdentificativo = $parse($searchInArray[ii])(item);
									var keywords = StringService.extractKeywords(textoIdentificativo);
									for ( var j in keywords ){
										if ( !keywords[j] ) continue;	
										
										var keyword = StringService.removeAccents(keywords[j].toString()).toLowerCase();
										
										distance = Math.min( StringService.levenshtein(keyword, word), distance );

										if ( keyword.indexOf(word) !== -1 ){
											distance = Math.min(distance, 0.5);
										}
									}
								}
							}
						}

						weight+=100/Math.max(1,distance*1.618);
					}
					weights.push(weight);
					$index+=1;
				}
				
				return $index;
			}
			
			//Una vez que tenemos el mapa de pesos, resolvemos la bÃƒÂºsqueda y devolvemos la conexiÃƒÂ³n
			function sortByMatches(collection, weights){
				var weightsMap = [];
				var maxWeight = Math.max.apply(Math, weights);

				var filteredCollection = collection.filter(function(item, index){
					var weight = weights[index];
					if ( weight*1.618 >= maxWeight ){
						weightsMap.push(weight);
						return true;
					}
					//if ( weight > 1 ) return true;
					else return false;
				});
				
				var weightsMap = weightsMap.map(function(w, index){
					return {
						weight:w,
						index:index
					}
				});

				weightsMap.sort(function(a, b){
					if (a.weight > b.weight) return -1;
					else if (b.weight > a.weight) return 1; 
					else return 0;
				});

				var sortedCollection = [];
				for ( var i = 0; i < weightsMap.length; i++ ){
					var index = weightsMap[i].index;
					sortedCollection.push(filteredCollection[index]);
				}
				
				return sortedCollection;
			}

			//Ordenar por campos
			function sortByFields(collection, sortBy){
				var $sortArray = sortBy.split(",").map(function(a){ return a.trim(); });
				
				return collection.slice().sort(function(a, b){		
					for ( var i = 0; i < $sortArray.length; i++ ){
						var sort = $sortArray[i];
						var lc, aVal, bVal;
						
						if ( sort.charAt(0) == "-" ){
							aVal = $parse(sort.substring(1))(a);
							bVal = $parse(sort.substring(1))(b);
						}
						else{
							aVal = $parse(sort)(a);
							bVal = $parse(sort)(b);
						}
						
						aVal = aVal === undefined ? "0" : aVal === null ? "0" : aVal === false ? "0" : aVal === true ? "1": aVal.toString();
						bVal = bVal === undefined ? "0" : bVal === null ? "0" : bVal === false ? "0" : bVal === true ? "1": bVal.toString();
						
						if ( sort.charAt(0) == "-" ) lc = bVal.localeCompare(aVal);
						else lc =  aVal.localeCompare(bVal);
						if ( lc != 0 ) return lc;
					}
					return 0;	
				});
			}
			
			//El buscador
			function Searcher(options){
				var self = this;
				self.isSearching = false;
				self.currentSearch = undefined;
				self.options = options || {};
				buildOptions(self.options);
				self.collection = [];
				self.total = undefined;
				self.first = undefined;
				self.last = undefined;
				self.page = self.options.page;
				self.orderBy = self.options.orderBy;
				self.filteredCollection = [];
				self.text = "";
				self.results = [];
				
				
				self.setSource = function(collection){
					self.collection = collection;
					self.search();
				}

				self.resolve = function(){
					self.total = self.filteredCollection.length;
					self.first = Math.min((self.page-1)*self.options.limit+1, self.total);
					self.last = Math.min(self.total, self.options.limit*self.page);
					self.results = self.filteredCollection.slice(self.first-1,self.last);
					self.currentSearch.resolve(self.results);
					self.isSearching = false;
				}
				
				self.sort = function(){
					self.filteredCollection = sortByFields(self.filteredCollection, self.orderBy);
					self.resolve();
				}

				self.search = function(){
					if ( self.isSearching ) self.currentSearch.reject();
					self.isSearching = true;
					self.currentSearch = $q.defer();

					if ( self.text == "" ){
						self.filteredCollection = self.collection;
						
						if (self.orderBy){
							self.sort();
						}
						else{
							self.resolve();	
						}
					}
					else{
						var $index = 0;
						var wordsSearched = StringService.extractKeywords(self.text);
						var weightsMap = [];
						
						self.tick = function(){
							if ( $index >= self.collection.length ){
								var filteredCollection = sortByMatches(self.collection, weightsMap);
								self.filteredCollection = filteredCollection;
								self.resolve();	
							}
							else{
								$index = buildWeightMap(self.collection, wordsSearched, $index, weightsMap, self.options);
								$timeout(function(){
									self.tick();
								},0);
							}
						}
						
						$timeout(function(){
							self.tick();
						}, 150);
					}

					return self.currentSearch.promise;
				}
			
				self.search();
			}

			return {
				searcher: Searcher
			}
		});
	}
})();

angular.module('ngControls', ['ngControlsSearch']);
/**
Declaramos la ruta relativa del mÃƒÂ³dulo para saber donde ir a buscar las plantillas
**/
(function(){ 'use strict' //PATH
	var scripts = document.getElementsByTagName("script");
	angular.module('ngControls').value("PATH", scripts[scripts.length-1].src.substring(0, scripts[scripts.length-1].src.lastIndexOf('/') + 1));
})();

/**
	Un servicio sencillo, que devuelve una plantilla dado su nombre, para ahorrar escribir un poquillo
**/
(function(){ 'use strict' //tplService
	angular.module('ngControls').factory('tplService', function(PATH){ 
		return {
			tplUrl:function(name){
				return PATH + name + ".html";
			}
		}
	})
})();

/**
Declaramos la proporciÃƒÂ³n ÃƒÂ¡urea, que utilizaremos en varias plantillas
**/
(function(){ 'use strict' //PHI
	angular.module('ngControls').value("PHI", 1.61803);
})();

/**
Servicio para realizar operaciones y validaciones con dnis/cifs/nifs/pasaportes
**/
(function(){ 'use strict' //NIF
	angular.module('ngControls').factory('NIF', function(){
		var letters = ["T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"];
		var cifLetters = ["J", "A", "B", "C", "D", "E", "F", "G", "H", "I"];
		
		return {
			getType:function(nif){
				if ( nif.match(/^\d{8}[-\s]?[A-Za-z]$/i) ) return "dni";
				else if ( nif.match(/^[XYZ][-\s]?\d{7}[-\s]?[A-Za-z]$/i) ) return "pasaporte";
				else if ( nif.match(/^[KPQSW][-\s]?((\d{2})(\d{5}))([A-Z])$|^[ABEH][-\s]?((\d{2})(\d{5}))(\d)$|^[CDFGJLMNRUV][-\s]?((\d{2})(\d{5}))(\d)$/i) ) return "cif";
				else return undefined;
			},
			getLetterAndNumber:function(nif, tipo){
				if ( tipo == "pasaporte" ){
					var matches = nif.match(/^([A-Za-z])[-\s]?(\d{7})[-\s]?([A-Za-z])$/i);
					
					var digitoControl = matches[1]; 
					var numero = digitoControl + matches[2];
					numero = numero.replace("X", '0').replace("Y", '1').replace("Z", '2');
					numero = parseInt(numero);
					
					var letra = matches[3];
				}
				else if( tipo == "dni" ) {
					var matches = nif.match(/^(\d{8})[-\s]?([A-Za-z])$/i);
					var numero = parseInt(matches[1]);
					var letra = matches[2];
				}
				else if( tipo == "cif" ){
					var matches = nif.match(/^([A-Z])[-\s]?((\d{2})\d{5})([A-Z]|\d)$/i);
					
					var tipoSociedad = matches[1];
					var numero = matches[2];
					var digitoControl = matches[4].match(/^[A-Z]$/i)? matches[4]:cifLetters[parseInt(matches[4])];
					var codigoProvincia = matches[3];
					
					return [digitoControl, numero, tipoSociedad, codigoProvincia];
				}
				else return false;
				
				return [letra, numero];
			},
			validate:function(nif/*, tipo = [dni|pasaporte|cif]*/){
				var tipo;
				if ( arguments.length > 1 ){
					tipo = arguments[1];
					if ( tipo.toLowerCase() == "nif" ) tipo = this.getType(nif);
					else if ( tipo.toLowerCase() != this.getType(nif) ) return false;
				}
				else tipo = this.getType(nif);
				if ( !tipo ) return false;
				
				var arr = this.getLetterAndNumber(nif, tipo);
				if ( !arr || !arr[0] ) throw "No se han podido extraer los dÃƒÂ­gitos de control del "+tipo+" " + nif;
				var letra = arr[0];

				return letra.toLowerCase() == this.calcLetter(nif, tipo).toLowerCase();
			},
			calcLetter:function(nif, tipo){
				var arr = this.getLetterAndNumber(nif, tipo);
				
				if ( tipo == "cif" ){
					var sumaPares = 0;
					for ( var i = 1; i < arr[1].length; i+=2 ) sumaPares+=parseInt(arr[1].charAt(i));
					
					var sumaImpares = 0;
					for ( var i = 0; i < arr[1].length; i+=2 ){
						var str = (parseInt(arr[1].charAt(i)*2)).toString();
						var val = 0;
						for ( var j = 0; j < str.length; j++ ) val+=parseInt(str.charAt(j));
						sumaImpares+=val;
					}

					var sumaTotal = (sumaPares+sumaImpares).toString();
					var d = parseInt(sumaTotal.charAt(sumaTotal.length - 1));

					if ( d == 0 ) var i = 0;
					else i = 10 - d;
					
					return cifLetters[i];
				}
				else{
					var numero = arr[1];
					var divisor = 23;
					
					var i = numero % divisor;
					return letters[i];
				}	
			}
		}
	})
})();

/**
Manejo de errores y validaciones
**/
(function(){ 'use strict' //CUSTOM VALIDATION
	angular.module('ngControls').factory('Validator', function(PATH, ngDate){ 
		//Pillamos el idioma del navegador para utilizarlo en mensajes de error, etc...
		var lang = navigator.language.substr(0,2) || navigator.userLanguage.substr(0,2);
		//La gestiÃƒÂ³n de errores, bastante self explained.
		var E_UNKNOWN = 0, E_REQUIRED = 1, E_NAN = 2, E_MIN = 3, E_MAX = 4, E_EMAIL = 5, E_MINLEN = 6, E_MAXLEN = 7, E_PATTERN = 8, E_BLACKLIST = 9, E_EQUAL = 10, E_INITIALDATE = 11, 
			E_NIF = 12, E_DNI = 13, E_CIF = 14, E_PASSPORT = 15, E_ENDDATE = 16, E_FILETYPE = 17, E_FILESIZE = 18, E_DATE = 19;	
		var ERR_MESSAGES = [
			{'es':'Error desconocido', 'en':'Unknown Error'}, 
			{'es':'Este campo es obligatorio', 'en':'This field is mandatory'}, 
			{'es':'Este campo debe ser numérico', 'en':'This field should be numeric'}, 
			{'es':'El valor es inferior al mínimo', 'en':'Value is lower than the minimum'}, 
			{'es':'El valor es superior al máximo', 'en':'Value is higher than the maximum'}, 
			{'es':'El formato de email no es válido', 'en':'Email format incorrect'},
			{'es':'No cumple longitud mínima', 'en':'Minimum length not matched'}, 
			{'es':'No cumple longitud máxima', 'en':'Maximum length not matched'}, 
			{'es':'El formato no es válido', 'en':'Format incorrect'}, 
			{'es':'Este valor no está permitido', 'en':'This value is not allowed'}, 
			{'es':'Los campos no coinciden', 'en':'Fields doesn´t match'},
			{'es':'La fecha es anterior a la fecha inicial', 'en':'This date is previous to the initial date'},
			{'es':'El formato de NIF no es correcto', 'en':'ID Format is not correct'},
			{'es':'El formato de DNI no es correcto', 'en':'ID Format is not correct'},
			{'es':'El formato de CIF no es correcto', 'en':'ID Format is not correct'},
			{'es':'El formato de Pasaporte no es correcto', 'en':'Passport Format is not correct'},
			{'es':'La fecha es posterior a la fecha final', 'en':'This date is higher to the end date'},
			{'es':'Formato de archivo incorrecto', 'en':'File format incorrect'},
			{'es':'El tamaño del archivo es demasiado grande', 'en':'File size too big'},
			{'es':'La fecha tiene un formato incorrecto'}
		];
		var getErrMsg = function(err){
			if (err){
				switch (err){
					case "required": return ERR_MESSAGES[E_REQUIRED][lang]; break;
					case "number": return ERR_MESSAGES[E_NAN][lang]; break; 
					case "min": return ERR_MESSAGES[E_MIN][lang]; break;
					case "max": return ERR_MESSAGES[E_MAX][lang]; break;
					case "email": return ERR_MESSAGES[E_EMAIL][lang]; break;
					case "minlength": return ERR_MESSAGES[E_MINLEN][lang]; break;
					case "maxlength": return ERR_MESSAGES[E_MAXLEN][lang]; break;
					case "pattern": return ERR_MESSAGES[E_PATTERN][lang]; break;
					case "blacklist": return ERR_MESSAGES[E_BLACKLIST][lang]; break;
					case "equals": return ERR_MESSAGES[E_EQUAL][lang]; break;
					case "initial-date": return ERR_MESSAGES[E_INITIALDATE][lang]; break;
					case "end-date": return ERR_MESSAGES[E_ENDDATE][lang]; break;
					case "nif": return ERR_MESSAGES[E_NIF][lang]; break;
					case "dni": return ERR_MESSAGES[E_DNI][lang]; break;
					case "cif": return ERR_MESSAGES[E_CIF][lang]; break;
					case "pasaporte": return ERR_MESSAGES[E_PASSPORT][lang]; break;
					case "mime": return  ERR_MESSAGES[E_FILETYPE][lang]; break;
					case "filesize": return  ERR_MESSAGES[E_FILESIZE][lang]; break;
					case "date": return ERR_MESSAGES[E_DATE][lang]; break;
					default: return ERR_MESSAGES[E_UNKNOWN][lang]; break;
				}
			}
			else return undefined;
		} 
		
		var validateEmail = function(email){
			var regex = new RegExp(/^[A-Za-z][A-Za-z0-9_\.]*[A-Za-z0-9_]@[A-Za-z0-9][A-Za-z0-9_\.]*\.[A-Za-z0-9]{2,}$/);
			return regex.test(email);
		}
		var validateMin = function(value, min, format){
			if ( typeof format == "undefined" ) format = "number";
			switch ( format ){
				case "date": return ngDate.strotime(value) >= ngDate.strotime(min); break;
				default: return parseFloat(value) >= parseFloat(min);
			}	
		}
		var validateMax = function(value, max, format){
			if ( typeof format === "undefined" ) format = "number";
			switch ( format ){
				case "date": return ngDate.strotime(value) <= ngDate.strotime(max); break;
				default: return parseFloat(value) <= parseFloat(max);
			}	
		}

		var validateDate = function(value){
			return ngDate.strtodate(value) ? true : false;
		}

		return {
			getErrMsg:getErrMsg,
			validateEmail:validateEmail,
			validateMin:validateMin,
			validateMax:validateMax,
			validateDate:validateDate
		}
	});

	//Error cuando dos inputs no tienen el mismo valor
	angular.module('ngControls').directive("equals", function(){
		return{
			require: '?ngModel',
			restrict:"A",
			link:function(scope, element, attrs, ngModel){
				scope.$watch(attrs.ngModel, function() {
					validate();
				});
				// observe the other value and re-validate on change
				attrs.$observe('equals', function (val) {
					validate();
				});
				var validate = function() {
					var val1 = ngModel.$viewValue;
					var val2 = scope.$eval(attrs.equals);
					ngModel.$setValidity('equals', ! val1 || ! val2 || val1 === val2);
				}
			}
		}
	})
})();

/**
**/
(function(){ 'use strict' //FILE
	angular.module('ngControls').factory('FileService', function($q){ //Un servicio para manejar archivos
		return {
			buildBlobUrl:function(file){
				var deferred = $q.defer();
				
				var reader = new FileReader();
				reader.readAsArrayBuffer(file);
				reader.onload = function (evt) {
					var blob = new Blob([evt.target.result], {type: file.type});
					var blob_url = URL.createObjectURL(blob);
					deferred.resolve(blob_url);
				};
				reader.onerror = function (evt) {
					deferred.reject(err);
				};
				return deferred.promise;
			}
		}
	})
})();

/**
ac-input: Tipo customizado de input con aspecto inspirado en material design;
tiene una gestiÃƒÂ³n personalizada de errores y aÃƒÂ±adimos varios tipos de inputs y validaciones personalizadas
**/
(function(){ 'use strict' //AC-INPUT
	angular.module('ngControls').directive('acInput', function($compile, tplService, NIF, PHI, Validator, $injector, $timeout, ngDate){
		String.prototype.replaceAll = function(str1, str2, ignore) 
		{
		    return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
		} 
		var acInputLinkFn = function($scope, $element, $attrs, ctrl){
			var input = $element.find("input");
			$scope.ngDate = ngDate;
			$scope.size = $scope.size ? $scope.size : 15;
			$scope.type = $scope.type ? $scope.type.toLowerCase():"text";
			$scope.PHI = PHI;
			$scope.disabled = false;
			
			if ( $scope.type == "date" ){
				$(input).datepicker(
				{
					dateFormat:'dd-mm-yy',
				    dayNames: ['Domingo','Lunes','Martes','MiÃƒÂ©rcoles','Jueves','Viernes','SÃƒÂ¡bado'],
				    dayNamesShort: ['Dom','Lun','Mar','Mie','Jue','Vie','Sab'],
				    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa'],
				    firstDay: 1,
				    monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
				    monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic']
				});
				$(input).on( "change", function() {
					if(!Validator.validateDate(input[0].value)){ ctrl.$setValidity("date", false); return false; }
					else ctrl.$setValidity("date", true);

					$scope.$apply(function(){
						$scope.model = ngDate.format("Y-m-d", input[0].value);
					});
			    });

				if ( $scope.model ) $scope.model = ngDate.format("Y-m-d", $scope.model);
				if ( $scope.model ) input.val(ngDate.format("d-m-Y", $scope.model));
			}
			
			if ( "height" in $element[0].style && $element[0].style.height ) $scope.height = $element[0].style.height;
			else $scope.height = $scope.size*2 + $scope.size*PHI + "px";
			
			$scope.inputBounds = [0, $scope.size*PHI / 2 + "px", "auto", $scope.size * 2 + "px"];
			$scope.inputPad = [$scope.size / PHI + "px", $scope.size +"px", 0, $scope.size / (PHI*PHI*PHI) + "px"];
			
			
			var runValidators = function(value){
				if ( $scope.type == "email" )
				if ( value ) ctrl.$setValidity("email", Validator.validateEmail(value));
				else ctrl.$setValidity("email", true);
				
				if ( $scope.type == 'nif' || $scope.type == 'cif' || $scope.type == 'dni' || $scope.type == 'pasaporte')
				if ( value ) ctrl.$setValidity($scope.type, NIF.validate(value, $scope.type));
				else ctrl.$setValidity($scope.type, true);
				
				if ( $attrs.min )
				if ( value ) ctrl.$setValidity("min", Validator.validateMin(value, $attrs.min, $scope.type));
				else ctrl.$setValidity("min", true);

				if ( $attrs.max )
				if ( value ) ctrl.$setValidity("max", Validator.validateMax(value, $attrs.max, $scope.type));
				else ctrl.$setValidity("max", true);
			}

			var applyMask = function(){
				var val =  input.val();
				var skip = 0;
				var maskedVal = "";
				for ( var i = 0; i < $scope.mask.length; i++ ){
					if ( !val[i - skip] ) break;
					var expect = $scope.mask[i];
					var expectNext = $scope.mask[i+1];
					
					if ( expect == 9 && /[0-9]/.test(val[i - skip]) ) maskedVal+=val[i - skip];
					else if ( expect == "A" && /[A-Za-z]/.test(val[i - skip]) ) maskedVal+=val[i - skip];
					else if ( expect == "@" && /[A-Za-z0-9]/.test(val[i - skip]) ) maskedVal+=val[i - skip];
					//else if ( expect == "A" && /\p{L}/.test(val[i - skip]) ) maskedVal+=val[i - skip];
					else if ( expect != "A" && expect != "9" && expect != "@" && expect != val[i - skip] ){
						maskedVal+= expect;
						
						if ( expectNext == 9 && /[0-9]/.test(val[i - skip]) ) skip+=1;
						else if ( expectNext == "A" && /[A-Za-z]/.test(val[i - skip]) ) skip+=1;
						else if ( expectNext == "@" && /[A-Za-z0-9]/.test(val[i - skip]) ) skip+=1;
						
						console.debug(expect);
						console.debug(val[i-skip]);
						//maskedVal+= expect;
						//skip+=1;
					}
					else if ( (expect == "A" || expect == "9" || expect == "@") && expect != val[i - skip] ){
						//Forzamos la siguiente iteraciÃƒÂ³n un caracter mÃƒÂ¡s adelante (al mantener i y reducir skip, repetimos la misma iteraciÃƒÂ³n, pero adelantamos en 1 el puntero al carÃƒÂ¡cter)
						skip-=1;
						i-=1;
						/*
						do{
							
						}while ( i - skip < val.length )
							*/
						//skip+=1;
						//i-=1;
						//break;
					}
					else maskedVal+= expect;
				}
				val = maskedVal;
				input.val(val);
				if (ctrl) ctrl.$setViewValue(val);
			}
			if ($scope.mask) input.on("input", applyMask);
			
			input.on("keyup keypress blur change", function(event){
				if( $scope.type != "date" ){
					$scope.$apply(function(){
						var val =  input.val();
						if (ctrl) ctrl.$setViewValue(val);
						$scope.value = val;
					});
				}
			})
			input.on("blur", function(){
				$scope.$apply(function(){
					$scope.ngBlur();
					if ( ctrl ){
						if (ctrl.$touched) return;
						else{
							ctrl.$setTouched();
						}
					}		
				});
			});

			input.on("focus", function(){
				$scope.$apply(function(){
					$scope.ngFocus();
				});
			})
		

			//Si instanciamos ngModel, vamos a sincronizar con el input, y controlaremos los errores
			if ( ctrl ){
				$scope.$watchCollection(function(){ return ctrl.$touched ? ctrl.$error: {}; }, function(newVal, oldVal){
					if ( ctrl.$touched ){
						if ( newVal !== oldVal ){
							var err = null;
							for ( var i in ctrl.$error ){
								if ( ctrl.$error[i] ) {
									err = i;
									break;
								}
							}
							$scope.err = Validator.getErrMsg(err);
						}
					}
				})
	
				$scope.$watch("value", function(newVal, oldVal){
					if ( $scope.value != input.val() && $scope.type != "date" ){	
						$scope.value = input.val();
						
						if ($scope.mask){
							$timeout(applyMask, 0);
						}
					}
					runValidators(newVal);
				});
				$scope.$watch("model", function(newVal, oldVal){
					if ($scope.value != $scope.model && $scope.type != "date"){
						$scope.value = $scope.model;
						input.val($scope.value);
					}
				});
			}
	
			
			$attrs.$observe("disabled", function(newVal, oldVal){
				if ( newVal !== undefined && newVal !== false ){
					$scope.disabled = true;
				}
				else{
					$scope.disabled = false;
				}
			})
		}
		var acInputFloatLinkFn = function($scope, $element, $attrs, ctrl){
			var input = $element.find("input");
			$scope.ngDate = ngDate;
			$scope.size = $scope.size ? $scope.size : 15;
			$scope.PHI = PHI;
			$scope.disabled = false;
			
			if ( "height" in $element[0].style && $element[0].style.height ) $scope.height = $element[0].style.height;
			else $scope.height = $scope.size*2 + $scope.size*PHI + "px";
			
			$scope.inputBounds = [0, $scope.size*PHI / 2 + "px", "auto", $scope.size * 2 + "px"];
			$scope.inputPad = [$scope.size / PHI + "px", $scope.size +"px", 0, $scope.size / (PHI*PHI*PHI) + "px"];
			
			
			var runValidators = function(value){
				if(value) ctrl.$setValidity("text", !isNaN(parseFloat(value)));
				else ctrl.$setValidity("text", true);
			}
			
			input.on("keyup keypress blur change", function(event){
				var val = input.val();
				if(event.type == "keyup" && (event.keyCode < 37 || event.keyCode > 40)){
					if(val.indexOf(",") > -1){
						var arrNumber = val.split(",");
					}

					var ret = typeof arrNumber != "undefined" ? arrNumber[0] : val;

					ret = ret.replaceAll(".", "");
					var flag_negativo = ret.indexOf("-") > -1 ? true : false;
					if(flag_negativo) ret = ret.replace("-", "");
					var res = "";
					if(ret && ret.length > 3){
						var count = 1;
						for(var i = ret.length-1; i > -1; i--){
							if(count % 3 == 0 && i > 0) res = "."+ret[i]+res;
							else res = ret[i]+res;
							count++;
						}
					}else{
						res = ret;
					}
					//if(typeof arrNumber != "undefined" && arrNumber[1] == "") arrNumber[1] = "0";
					val = typeof arrNumber != "undefined" ? res+','+arrNumber[1] : res;

					if(flag_negativo) val = "-"+val;
				}

				input.val(val);
				$scope.$apply(function(){
					$scope.model = val.replaceAll(".", "").replace(",", ".");
				});
			})
			input.on("blur", function(){
				$scope.$apply(function(){
					$scope.ngBlur();
					if ( ctrl ){
						if (ctrl.$touched) return;
						else{
							ctrl.$setTouched();
						}
					}		
				});
			});

			input.on("focus", function(){
				$scope.$apply(function(){
					$scope.ngFocus();
				});
			})
		

			//Si instanciamos ngModel, vamos a sincronizar con el input, y controlaremos los errores
			if ( ctrl ){
				$scope.$watchCollection(function(){ return ctrl.$touched ? ctrl.$error: {}; }, function(newVal, oldVal){
					if ( ctrl.$touched ){
						if ( newVal !== oldVal ){
							var err = null;
							for ( var i in ctrl.$error ){
								if ( ctrl.$error[i] ) {
									err = i;
									break;
								}
							}
							$scope.err = err ? "Formato incorrecto" : "";
						}
					}
				})
	
				$scope.$watch("value", function(newVal, oldVal){
					if ( $scope.value != input.val() ){	
						$scope.value = input.val();
					}

					runValidators(newVal);
				});
				$scope.$watch("model", function(newVal, oldVal){
					if ($scope.value != $scope.model && $scope.type != "date" || !$scope.value){
						$scope.value = $scope.model;

						if($scope.value){
							var val = $scope.value.toString().replace(".", ",");

							if(val.indexOf(",") > -1){
								var arrNumber = val.split(",");
							}

							var ret = typeof arrNumber != "undefined" ? arrNumber[0] : val;

							var flag_negativo = ret.indexOf("-") > -1 ? true : false;
							if(flag_negativo) ret = ret.replace("-", "");
							var res = "";
							if(ret && ret.length > 3){
								var count = 1;
								for(var i = ret.length-1; i > -1; i--){
									if(count % 3 == 0 && i > 0) res = "."+ret[i]+res;
									else res = ret[i]+res;
									count++;
								}
							}else{
								res = ret;
							}
							//if(typeof arrNumber != "undefined" && arrNumber[1] == "") arrNumber[1] = "0";
							val = typeof arrNumber != "undefined" ? res+','+arrNumber[1] : res;

							if(flag_negativo) val = "-"+val;

							input.val(val);
						}
					}
				});
			}
	
			
			$attrs.$observe("disabled", function(newVal, oldVal){
				if ( newVal !== undefined && newVal !== false ){
					$scope.disabled = true;
				}
				else{
					$scope.disabled = false;
				}
			})
		}
		var acCheckboxLinkFn = function($scope, $element, $attrs, ctrl){
			$scope.size = $scope.size ? $scope.size : 15;
			$scope.PHI = PHI;
			$scope.disabled = false;
			if ( ctrl ){
				ctrl.$isEmpty = function(){
					return !$scope.isTrue($scope.value);
				}
			}

			$attrs.$observe("disabled", function(newVal, oldVal){
				if ( newVal !== undefined && newVal !== false ){
					$scope.disabled = true;
				}
				else{
					$scope.disabled = false;
				}
			})
	
			if ( "trueValue" in $attrs ) $scope.trueValue = $attrs.trueValue;
			else $scope.trueValue = true;
			
			if ( "falseValue" in $attrs ) $scope.falseValue = $attrs.falseValue;
			else $scope.falseValue = false;

			$scope.isTrue = function(value){
				if ( $scope.trueValue === true ) return !(value === undefined || value === 0 || value === false || value === "0" || value === null || value === null || value === "");
				else return $scope.trueValue == $scope.value;
			}	
			$scope.toggle = function(){
				if ( $scope.isTrue($scope.value) ){
					$scope.value = $scope.falseValue;
				}
				else{
					$scope.value = $scope.trueValue;
				}
				
				if ( ctrl ){
					if (!ctrl.$touched) ctrl.$setTouched();
					ctrl.$setViewValue($scope.value);
				}
			}
	
			var runValidators = function(value){
				return true;
			}

			//Si instanciamos ngModel, vamos a sincronizar con el input, y controlaremos los errores
			if ( ctrl ){
				$scope.$watchCollection(function(){ return ctrl.$touched ? ctrl.$error: {}; }, function(newVal, oldVal){
					if ( ctrl.$touched ){
						if ( newVal !== oldVal ){
							var err = null;
							for ( var i in ctrl.$error ){
								if ( ctrl.$error[i] ) {
									err = i;
									break;
								}
							}
							$scope.err = Validator.getErrMsg(err);
						}
					}
				})
	
				$scope.$watch("value", function(newVal, oldVal){
					runValidators(newVal);
				});
				$scope.$watch("model", function(newVal, oldVal){
					if ($scope.value != $scope.model) $scope.value = $scope.model;
				});
			}
		}
		var acFileLinkFn = function($scope, $element, $attrs, ctrl){
			var input = $element.find("input")[0];
			var FileService = $injector.get("FileService");
			var $window = $injector.get("$window");

			$scope.size = $scope.size ? $scope.size : 15;
			$scope.PHI = PHI;
			if ( $attrs.maxSize ){
				var matches = $attrs.maxSize.match(/(\d{1,})([K|M|T])b?$/i);
				switch(matches[2].toLowerCase()){
					case "k" : $scope.maxSize = matches[1]*1024; break;
					case "m" : $scope.maxSize = matches[1]*1024*1024; break;
					case "g" : $scope.maxSize = matches[1]*1024*1024*1024; break;
					case "t" : $scope.maxSize = matches[1]*1024*1024*1024*1024; break;
					default: $scope.maxSize = matches[1];
				}
			}
			
			var runValidators = function(value){
				if ( $scope.accept )
				if ( $scope.mimeType !== undefined ) ctrl.$setValidity("mime", $scope.mimeType == $scope.accept);
				else ctrl.$setValidity("mime", true);
				
				if ( $scope.maxSize )
				if ( $scope.fileSize !== undefined ) ctrl.$setValidity("filesize", $scope.fileSize <= $scope.maxSize);
				else ctrl.$setValidity("filesize", true);	
			}
			
			
			$element.find("a").on("click", function(event){
				input.click();
			});
			input.addEventListener("change", function(event){
				ctrl.$setTouched();
			
				var target = event.target;
				if ( target.files.length ){
					var file = target.files[0];
					ctrl.$setViewValue(file);
					FileService.buildBlobUrl(file).then(function(blobUrl){
						$scope.url = blobUrl;
						$scope.value = file.name;
						$scope.mimeType = file.type;
						$scope.fileSize = file.size;
					})
				}
				else{
					$scope.$apply(function(){
						ctrl.$setViewValue(undefined);
						$scope.url = undefined;
						$scope.value = undefined;
						$scope.mimeType = undefined;
						$scope.fileSize = undefined;
					})
				}
			});
			
			$scope.preview = function(){
				$window.open($scope.url);
			}
		
			if ( ctrl ){
				$scope.$watchCollection(function(){ return ctrl.$touched ? ctrl.$error: {}; }, function(newVal, oldVal){
					if ( ctrl.$touched ){
						if ( newVal !== oldVal ){
							var err = null;
							for ( var i in ctrl.$error ){
								if ( ctrl.$error[i] ) {
									err = i;
									break;
								}
							}
							$scope.err = Validator.getErrMsg(err);
						}
					}
				})
	
				$scope.$watch("value", function(newVal, oldVal){
					runValidators(newVal);
				});
				$scope.$watch("model", function(newVal, oldVal){
					if ($scope.value != $scope.model) $scope.value = $scope.model;
				});
			}
		}
		var acImageLinkFn = function($scope, $element, $attrs, ctrl){
			var input = $element.find("input")[0];
			var FileService = $injector.get("FileService");
			var $window = $injector.get("$window");

			$scope.size = $attrs.size;
			$scope.width = $attrs.width;
			$scope.height = $attrs.height;
			//if ( !$scope.placeholder ) $scope.placeholder = 'Subir foto';
			
			var imgWrapper = $element.find("div");

			if ( $scope.size ){
				if ( $scope.size instanceof Array ){
						$scope.width = $scope.size[0];
						$scope.height = $scope.size[1];
				}
				else{
					$scope.width = $scope.size;
					$scope.height = $scope.size;
				}
			}
			$scope.PHI = PHI;
			
			//mÃƒÂ¡ximo tamaÃƒÂ±o de archivo
			if ( $attrs.maxSize ){
				var matches = $attrs.maxSize.match(/(\d{1,})([K|M|T])b?$/i);
				switch(matches[2].toLowerCase()){
					case "k" : $scope.maxSize = matches[1]*1024; break;
					case "m" : $scope.maxSize = matches[1]*1024*1024; break;
					case "g" : $scope.maxSize = matches[1]*1024*1024*1024; break;
					case "t" : $scope.maxSize = matches[1]*1024*1024*1024*1024; break;
					default: $scope.maxSize = matches[1];
				}
			}
			
			//Cuando se sube la imagen, si queremos que el tamaÃƒÂ±o del input se adapte al contenido de la imagen
			if ( "resize" in $attrs ) $scope.resize = true;
			else $scope.resize = false;
			
			//Esta funciÃƒÂ³n valida que sea una imagen el valor asignado y que cumpla con el tamaÃƒÂ±o asignado.
			var runValidators = function(value){
				if ( $scope.model instanceof File ) ctrl.$setValidity("mime", /^image/i.test($scope.model.type) );
				else ctrl.$setValidity("mime", true);

				if ( $scope.maxSize )
				if ( $scope.model instanceof File ) ctrl.$setValidity("filesize", $scope.model.size <= $scope.maxSize);
				else ctrl.$setValidity("filesize", true);	
			}

			//El constructor de la imagen: cuando el archivo cambia, la imagen cambia con el archivo.
			var render = function(){
				if ( $scope.model instanceof File ){
					FileService.buildBlobUrl($scope.model).then(function(blobUrl){
						$scope.value = blobUrl;
						var img = new Image();
						img.onload = function() {
							if ( $scope.resize ){
								var anchorWidth = $scope.width;
								var anchorHeight = $scope.height;
								
								var width = $element[0].getBoundingClientRect().width;
								var height = $element[0].getBoundingClientRect().height;
								
								var ratio;
								if ( anchorWidth && !anchorHeight ) ratio = width / this.width;
								else if ( !anchorWidth && anchorHeight ) ratio = height / this.height;
								else ratio = width / this.width; //Como normalmente necesitamos el parÃƒÂ¡metro altura para que se muestre, por defecto, anclamos a la anchura
								
								//ratio = Math.max( width / this.width, height / this.height );
								
								imgWrapper.css("width", this.width*ratio+"px");
								imgWrapper.css("height", this.height*ratio+"px");
							}
						}
						img.src = $scope.value;			
					})
				}
				else{
					$scope.value = $scope.model;
					if ( $scope.value ){
						var img = new Image();
						img.onload = function() {
							if ( $scope.resize ){
								var ratio = Math.max( $scope.width / this.width, $scope.height / this.height );
								imgWrapper.css("width", this.width*ratio+"px");
								imgWrapper.css("height", this.height*ratio+"px");
							}
						}
						img.src = $scope.value;		
					}
				}
			}
			
			//El manejo del input file
			$element.on("click", function(event){
				input.click();
				ctrl.$setTouched();
			});
			input.addEventListener("change", function(event){
				ctrl.$setTouched();
			
				var target = event.target;
				if ( target.files.length ){
					var file = target.files[0];
					ctrl.$setViewValue(file);		
				}
				else{
					$scope.$apply(function(){
						ctrl.$setViewValue(undefined);
						$scope.url = undefined;
						$scope.value = undefined;
						$scope.mimeType = undefined;
						$scope.fileSize = undefined;
						$scope.thumb = undefined;			
					})
				}
			});
			
			if ( ctrl ){
				$scope.$watchCollection(function(){ return ctrl.$touched ? ctrl.$error: {}; }, function(newVal, oldVal){
					if ( ctrl.$touched ){
						if ( newVal !== oldVal ){
							var err = null;
							for ( var i in ctrl.$error ){
								if ( ctrl.$error[i] ) {
									err = i;
									break;
								}
							}
							$scope.err = Validator.getErrMsg(err);
						}
					}
				})
	
				$scope.$watch("value", function(newVal, oldVal){
					runValidators(newVal);
				});
				$scope.$watch("model", function(newVal, oldVal){
					render();
				});
			}
		}
		
		return{
			replace:true,
			transclude:true,
			templateUrl: function($element, $attrs){	
				switch($attrs.type){
					case "file" : return tplService.tplUrl("ac-input--file"); break;
					case "image" : return tplService.tplUrl("ac-input--image"); break;
					case "checkbox" : return tplService.tplUrl("ac-input--checkbox"); break;
					case "date" : return tplService.tplUrl("ac-input--date"); break;
					case "float" : return tplService.tplUrl("ac-input--number-formated"); break;
					default : return tplService.tplUrl("ac-input");
				}
			},
			restrict:"EC",
			scope:{
				size:"=?",
				value:"@",
				placeholder:"@",
				type:"@",
				model:"=?ngModel",
				accept:"@",
				checked:"=?",
				mask:"@",
				ngFocus:"&",
				ngBlur:"&",
				plugin:"@?"
			},
			require:"?ngModel",
			link:function($scope, $element, $attrs, ctrl){
				switch($scope.type){
					case "file" : acFileLinkFn($scope, $element, $attrs, ctrl); break;
					case "image" : acImageLinkFn($scope, $element, $attrs, ctrl); break;
					case "checkbox" : acCheckboxLinkFn($scope, $element, $attrs, ctrl); break;
					case "float": acInputFloatLinkFn($scope, $element, $attrs, ctrl); break;
					default: acInputLinkFn($scope, $element, $attrs, ctrl); break;
				}
			}
		}
	})
	angular.module('ngControls').directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                    scope.$apply(function(){
                        scope.$eval(attrs.ngEnter, {'event': event});
                    });

                    event.preventDefault();
                }
            });
        };
    });
})();

(function(){ 'use strict' //AC-TEXTAREA
	angular.module('ngControls').directive('acTextarea', function($compile, tplService, NIF, PHI, Validator, $injector, $timeout, ngDate){
		var acTextareaLinkFn = function($scope, $element, $attr, ctrl){
			var textarea = $element.find("textarea");
			$scope.size = $scope.size ? $scope.size : 15;
			$scope.PHI = PHI;

			textarea.on("keyup keypress blur change", function(event){
				$scope.$apply(function(){
					var val =  textarea.val();
					if (ctrl) ctrl.$setViewValue(val);
					$scope.value = val;
				});
			})

			textarea.on("blur", function(){
				$scope.$apply(function(){
					$scope.ngBlur();
					if ( ctrl ){
						if (ctrl.$touched) return;
						else{
							ctrl.$setTouched();
						}
					}		
				});
			});

			textarea.on("focus", function(){
				$scope.$apply(function(){
					$scope.ngFocus();
				});
			})

			if ( ctrl ){
				$scope.$watchCollection(function(){ return ctrl.$touched ? ctrl.$error: {}; }, function(newVal, oldVal){
					if ( ctrl.$touched ){
						if ( newVal !== oldVal ){
							var err = null;
							for ( var i in ctrl.$error ){
								if ( ctrl.$error[i] ) {
									err = i;
									break;
								}
							}
							$scope.err = Validator.getErrMsg(err);
						}
					}
				})
	
				/*$scope.$watch("value", function(newVal, oldVal){
					if ( $scope.value != textarea.text() ){	
						$scope.value = textarea.text();
						
						if ($scope.mask){
							$timeout(applyMask, 0);
						}
					}
				});*/
				$scope.$watch("model", function(newVal, oldVal){
					if ($scope.value != $scope.model){
						$scope.value = $scope.model;
						textarea.text($scope.value);
					}
				});

				$attr.$observe("disabled", function(newVal, oldVal){
					$element.addClass("disabled");
				});
			}
		}
		
		return{
			replace:true,
			transclude:true,
			templateUrl: function($element, $attrs){
				return tplService.tplUrl("ac-textarea");
			},
			restrict:"EC",
			scope:{
				size:"=?",
				placeholder:"@",
				model:"=?ngModel",
				ngFocus:"&",
				ngBlur:"&"
			},
			require:"?ngModel",
			link:function($scope, $element, $attrs, ctrl){
				acTextareaLinkFn($scope, $element, $attrs, ctrl);
			}
		}
	})
})();

/**
**/
(function(){ 'use strict' //AC-RIPPLE
	angular.module('ngControls').directive("acRipple", function($timeout, $window, PHI){
		return {
			restrict:"EC",
			link:function($scope, $element, $attrs){
				if ( $attrs.rippleColor ) var rippleColor = $attrs.rippleColor;
				else var rippleColor = "rgba(0, 0, 0, 0.3)";
				/*if ( !$scope.size ) $scope.size = 15;

				$scope.style = {
					"font-size":$scope.size+"px",
					"padding":$scope.size /(PHI*PHI)+"px "+$scope.size+"px"
				}
				*/

				$element.on('click', function (event) {
					//event.preventDefault();
					
					var btnOffset = $element[0].getBoundingClientRect();
					var xPos = event.pageX - btnOffset.left;
					var yPos = event.pageY - btnOffset.top - $window.scrollY;

					var $div = angular.element('<div class = "ripple-effect"></div>');
					var $ripple = angular.element("<div class = 'ripple-effect'></div>");
					$div.css("height", btnOffset.height + "px");
					$div.css("width", btnOffset.height + "px");

					$element.append($div);

					$div.css({
						top: yPos - ($div[0].getBoundingClientRect().height/2) + "px",
						left: xPos - ($div[0].getBoundingClientRect().width/2) + "px",
						background: rippleColor
					});
					$timeout(function(){
						$div.remove();
					}, 1500);				
				});
			}
		}
	})
	angular.module('ngControls').directive("acRoundRipple", function($timeout, PHI){
		return {
			restrict:"EC",
			link:function($scope, $element, $attrs){
				if ( $attrs.rippleColor ) var rippleColor = $attrs.rippleColor;
				else var rippleColor = "rgba(0, 0, 0, 0.3)";

				$element.on('click', function (event) {
					//event.preventDefault();
					
					var btnOffset = $element[0].getBoundingClientRect();
					//var xPos = event.pageX - btnOffset.left;
					//var yPos = event.pageY - btnOffset.top;
					var xPos = btnOffset.width / 2;
					var yPos = btnOffset.height / 2;
					
					var $div = angular.element('<div class = "ripple-effect"></div>');
					var $ripple = angular.element("<div class = 'ripple-effect'></div>");
					$div.css("height", btnOffset.height + "px");
					$div.css("width", btnOffset.height + "px");

					$element.append($div);
					$div.css({
						top: yPos - ($div[0].getBoundingClientRect().height/2) + "px",
						left: xPos - ($div[0].getBoundingClientRect().width/2) + "px",
						background: rippleColor
					});
					$timeout(function(){
						$div.remove();
					}, 300);				
				});
			}
		}
	})
})();

/**
Un botÃƒÂ³n
**/
(function(){ 'use strict' //AC-BUTTON
	angular.module('ngControls').directive("acButton", function($timeout, PHI){
		return {
			restrict:"EC",
			scope:{
				size:"=?",
			},
			replace:true,
			transclude:true,
			template:"<button><div ng-transclude ng-style = 'style'></div></button>",
			link:function($scope, $element, $attrs){
				if ( !$scope.size ) $scope.size = 15;

				$scope.style = {
					"font-size":$scope.size+"px",
					"padding":$scope.size /(PHI*PHI)+"px "+$scope.size+"px"
				}
			}
		}
	})
})();

/**
**/
(function(){ 'use strict' //AC-SELECT
	//Un select con opciÃƒÂ³n de buscar, ordenar, limitar resultados, etc... y que ademÃƒÂ¡s es to bonico
	angular.module('ngControls').directive("acSelect", function(tplService, $templateRequest, $compile, $document, $q, $parse, $timeout, ngSearch, ngControlsSearch, PHI, PATH, Validator){
		var getBaseExpr = function(displayName, itemName){
			var regex = new RegExp(itemName+"\\.", "g");
			return displayName.replace(regex, "");
		}
		var OPT_REGEXP = /^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+group\s+by\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?$/;

		return {
			restrict: 'EC',
			templateUrl: function($element, $attrs){
				switch($attrs.type){
					default : return tplService.tplUrl("ac-select");
				}
			},
			transclude:true,
			replace:true,
			scope:{},
			bindToController:{
				placeholder:"@",
				type:"@",
				options:"@",
				value:"=ngModel",
				limit:"=",
				orderBy:"@",
				size:"=?",
				dropdownSize:"=?",
				theme:"@",
				template:"@",
				onTextChange:"&",
				onValueChange:"=",
				width:"@"
			},
			require: '?ngModel',
			controller:function($scope, $element, $attrs, $transclude){
				var self = this;

				//Lo primero, extraemos las funciones necesarios del atributo options
				var matches = self.options.match(OPT_REGEXP);
				//Construye un objeto local con el nombre correspondiente
				self.getLocals = function(item){ 
					var itemName = matches[4];
					var locals = {};
					locals[itemName] = item;
					return locals;
				}
				//Dado un item, devuelve su valor
				self.valueFn = function(item){
					var valueName = matches[1];
					var locals = self.getLocals(item);
					return $parse(matches[1])($scope, locals);
				}
				//Dado un item, devuelve su texto
				self.displayFn = function(item){
					var displayName = matches[2] || matches[1];
					var locals = self.getLocals(item);
					return $parse(displayName)($scope, locals);	
				}
				//El nombre de la colecciÃƒÂ³n.
				self.collectionName = matches[7];
				//La funciÃƒÂ³n que devuelve la colecciÃƒÂ³n completa de items
				self.collectionFn = function(){
					self.collectionName = matches[7]
					return $parse(self.collectionName)($scope.$parent) || [];
				}
				//La funciÃƒÂ³n que devuelve el trackBy
				self.trackByFn = function(item){
					var trackByName = matches[8] ? matches[8] : matches[1];
					var locals = self.getLocals(item);
					return $parse(trackByName)($scope, locals);
				}
				//EstÃƒÂ¡ funciÃƒÂ³n devuelve el ÃƒÂ­ndice de un elemento en el array de resultados (SÃƒÂ³lo multiples)
				self.indexFn = function(item){
					for ( var i = 0; i < self.value.length; i++ ){
						if ( angular.equals(item, self.value[i]) ||  angular.equals(self.valueFn(item), self.value[i])) return i;
					}
					return -1;
				}
				
				
				//Inicializaciones
				self.ready = false;
				self.open = false;
				self.size = self.size ? self.size : 15;
				self.firstIndex = self.selectedIndex = self.cursorIndex = 0;
				self.searchIn = getBaseExpr(matches[2] || matches[1], matches[4]);
				self.orderBy = self.orderBy ? self.orderBy : self.searchIn;
				self.valueMap = [];
				self.displayMap = [];
				self.boolMap = [];
				self.err = "";
				self.listeners = {ready:[]};
				self.multiple = ("multiple" in $attrs);
				if ( self.multiple && !self.value ) self.value = [];
				
				//Crear el buscador
				self.searcher = ngSearch.get({
					limit:self.limit,
					orderBy:self.orderBy,
					src:self.collectionFn,
					searchIn:self.searchIn,
				});
				self.searcher.on("search", function(text){});		
				self.searcher.on("resolve", function(results){
					self.valueMap = results.map(self.valueFn);
					self.displayMap = results.map(self.displayFn);	
					if ( self.multiple ){
						self.boolMap = results.map(function(item){
							return self.indexFn(self.valueFn(item)) != -1;
						});
					}	
				});

				//Acciones disponibles
				self.select = function($index){
					if ( self.multiple ){
						var pos = self.indexFn(self.valueFn(self.searcher.results[$index]));
						if ( pos != -1 ){ //Ya estaba, estamos quitando
							self.value.splice(pos, 1);
							self.boolMap[$index] = false;	
						}
						else{ //No estaba, estamas marcando
							self.value.splice($index, 0, self.valueMap[$index]);
							self.boolMap[$index] = true;	
						}
					}
					else{
						self.selectedIndex = $index;
						self.cursorIndex = $index;
						self.text = self.displayMap[$index];
						self.value = self.valueMap[$index];
						self.open = false;
					}
				}
				
				self.next = function(){
					self.cursorIndex = Math.min(self.cursorIndex+1, self.searcher.results.length - 1);
				}
				self.prev = function(){
					self.cursorIndex = Math.max(self.cursorIndex-1, self.firstIndex);
				}	
				self.show = function(){
					self.open = true;
				}
				self.hide = function(){
					self.open = false;
				}
			
				self.when = function(ev, callback){
					self.listeners[ev].push(callback);
				}
			},
			controllerAs:"vm",
			link:function($scope, $element, $attrs, ctrl, $transclude){
				var vm = $scope.vm;
				//Modificamos la funciÃƒÂ³n de ngModel "isEmpty" para que los arrays vacÃƒÂ­os, si es un campo obligatorio, sea considerado no vÃƒÂ¡lido
				if (ctrl && vm.multiple){
					ctrl.$isEmpty = function(value){ return value ? value.length == 0 : true; }
					if ( !ctrl.$options ) ctrl.$options = {}
					ctrl.$options.allowInvalid = true;
				}
				
				//Crear el dropdown
				var dropdown, tplUrl;
				if ( vm.multiple ) tplUrl = "ac-select-dropdown--multiple";
				else tplUrl = "ac-select-dropdown";
				$templateRequest(tplService.tplUrl(tplUrl)).then(function(html){
					var tpl = angular.element(html);
					var transcludedNodes = Array.prototype.slice.call($transclude()).filter(function(el){
						return angular.element(el).text().trim().length > 0;
					});
					for ( var i = transcludedNodes.length; i > 0; i-- ){
						var index = i - transcludedNodes.length - 1;
						var li = angular.element("<li />").append(transcludedNodes[i - 1]);
						li.attr('ng-style', '{padding:vm.size + \"px\"+\" \" + vm.size + \"px\"}');
						li.attr('ng-class', '{highlight:vm.cursorIndex == '+index+'}');
						li.addClass('ac-ripple');		
						tpl.find("ul").prepend(li);
					}
					vm.firstIndex = vm.cursorIndex = vm.selectedIndex = -transcludedNodes.length;
					dropdown = $compile(tpl)($scope);
					$document.find("body").append(dropdown);
					for ( var i = 0; i < vm.listeners.ready.length; i++ ){
						vm.listeners.ready[i].call();
					}
				});
				//dropdown image
				$element.find("img").attr("src", PATH + "loading.gif");
				
				
				//CSS:
				$element.css({
					fontSize: vm.size+"px",
					height: vm.size*2 + vm.size*PHI + "px"
				});
				var inputBounds = {
					top: vm.size*PHI / 2 + "px",
					height: vm.size * 2 + "px",
					paddingRight:vm.size*PHI + "px",
					paddingLeft:vm.size / (PHI*PHI*PHI) + "px",
				}
				$element.find("input").css(inputBounds);
				var placeholderBounds = {
					top: vm.size*PHI / 2 + "px",
					height: vm.size * 2 + "px",
					paddingRight:vm.size + "px",
					paddingLeft:vm.size / (PHI*PHI*PHI) + "px",
					paddingTop:vm.size / (PHI*PHI) + "px",
				}
				angular.element($element[0].querySelector(".ac-placeholder")).css(placeholderBounds);
				var errorBounds = {
					top: vm.size * 2 + vm.size*PHI / 2 + "px",
					paddingRight:vm.size + "px",
					paddingLeft:vm.size / (PHI*PHI*PHI) + "px",
				}
				angular.element($element[0].querySelector(".ac-error")).css(errorBounds);
				var arrowBounds = {
					top: vm.size + vm.size / (PHI*PHI) + "px",
					right: vm.size / (PHI*PHI) + "px",
					width: vm.size + "px",
					height: vm.size + "px"
				}
				angular.element($element[0].querySelector(".ac-arrow")).css(arrowBounds);
				var spinnerBounds = {
					top: vm.size + vm.size / (PHI*PHI) + "px",
					right: vm.size / (PHI*PHI) + "px",
					width: vm.size + "px",
					height: vm.size + "px"
				}
				angular.element($element[0].querySelector(".ac-spinner")).css(spinnerBounds);
				
				$attrs.$observe("disabled", function(newVal, oldVal){
					$element.addClass("disabled");
				});
				
				//DOM events
				var clickOut = function(event){
					var isChild = $element[0].contains(event.target) || (dropdown && dropdown[0].contains(event.target));
					var isSelf = $element[0] == event.target || (dropdown && dropdown[0] == event.target);

					if (!isChild && !isSelf){
						vm.hide();
						$scope.$apply();
					}		
				}
				$document.bind('click', clickOut);
				$element.bind("click", function(){
					vm.show();
					$scope.$apply();
				});
				
				
				var inputValue = $element.find("input")[0].value;
				$element.find("input").bind("keyup", function(event){
					switch( event.keyCode ){
						case 9: { //Tab
							vm.show();
							$scope.$apply();
						}
						break;
						case 13:{ //Enter
							vm.select(vm.cursorIndex); 
							$scope.$apply();
						}
						break;
						default:{
							if ( (vm.type == "search" && event.target.value != inputValue) || event.target.value == ""){
								vm.searcher.text = event.target.value;
								vm.value = vm.multiple ? [] : undefined;
								vm.onTextChange({texto:event.target.value});
								$scope.$apply();
							}
						}
						
					}
				});
				$element.find("input").bind("keydown", function(event){
					inputValue = event.target.value;
					switch( event.keyCode ){
						case 9: vm.hide(); break; //Tab
						case 38:{
							vm.prev(event); //Arrow up
							$scope.$apply();
						}
						break;
						case 40: {
							vm.next(event); //Arrow down
							$scope.$apply();
						}
						break;
						default: break;
					}
				});

				//Watchers
				vm.when("ready", function(){
					$scope.$watch("vm.cursorIndex", function(newVal){
						if ( vm.cursorIndex < 1 ) return;
						
						var position = newVal - vm.firstIndex;
						var elements = dropdown.find("li");
						if(position < elements.length - 1){
							var y = dropdown.find("li")[position].offsetTop;		
							dropdown[0].scrollTop = y - dropdown[0].getBoundingClientRect().height / 2;
						}
					});
					if(ctrl){
						$scope.$watchCollection(function(){ return ctrl.$touched ? ctrl.$error: {}; }, function(newVal, oldVal){
							if ( ctrl.$touched ){
								if ( newVal !== oldVal ){
									var err = null;
									for ( var i in ctrl.$error ){
										if ( ctrl.$error[i] ) {
											err = i;
											break;
										}
									}
									vm.err = Validator.getErrMsg(err);
								}
							}
						});
					}
					$scope.$watch("vm.open", function(newVal, oldVal){
						if ( newVal ){
							var rect = $element[0].getBoundingClientRect();

							if ( rect.top > window.innerHeight / 2 ){
								var rect = $element[0].querySelector("input").getBoundingClientRect();
								var bottom = window.innerHeight - rect.top;
								var width = vm.width && vm.width == "full" ? "calc(100% - "+rect.left+"px)" : rect.width+"px";
								dropdown.css({
									bottom:(bottom+19) + "px", //cuando el input se activa aplica una transformaciÃƒÂ³n vertical de 19px
									left:rect.left + "px",
									width:width
								});
								dropdown.addClass("visible");
							}
							else{
								var rect = $element[0].querySelector("input").getBoundingClientRect();
								var top = vm.type == "search" ? rect.top + rect.height - 19 : rect.top - 19;
								var width = vm.width && vm.width == "full" ? "calc(100% - "+rect.left+"px)" : rect.width+"px";
								dropdown.css({
									top:top + "px", //cuando el input se activa aplica una transformaciÃƒÂ³n vertical de 19px
									left:rect.left + "px",
									width:width
								});
								dropdown.addClass("visible");
							}
							
							$element.find("input")[0].focus();
							if ( ctrl ) ctrl.$setTouched();
						}
						else{
							dropdown.removeClass("visible");
							$element.find("input")[0].blur();
						}
					})
					
					if ( vm.multiple ){
						$scope.$watchCollection("vm.value", function(newVal, oldVal){
							var arr = [];
							for ( var i = 0; i < vm.searcher.collection.length; i++ ){
								var item = vm.searcher.collection[i];
								if ( vm.indexFn(vm.searcher.collection[i]) !== -1 ) arr.push(vm.displayFn(vm.searcher.collection[i])); 
							}
							vm.text = arr.join(", ");
							ctrl.$validate();
							if(vm.onValueChange) vm.onValueChange(newVal.filter(function(i) {return oldVal.indexOf(i) < 0;}));
						});

						//Si cambia el source revisamos si el model tiene datos que ya no están en el source
						$scope.$watch(function(){ return vm.valueMap; }, function(newVal, oldVal){
							var tmp = [];
							for(var i = 0; i < vm.value.length; i++){
								var esta = false;
								for(var j = 0; j < newVal.length; j++){
									if(typeof vm.value[i] == "object" && typeof newVal[j] == "object" && JSON.stringify(vm.value[i]) == JSON.stringify(newVal[j])) esta = true;
									else if(vm.value[i] == newVal[j]) esta = true;
								}
								if(esta) tmp.push(vm.value[i]);
							}
							vm.value = tmp;
							ctrl.$setViewValue(vm.value);
						});
					}
					else{



						$scope.$watch("vm.value", function(newVal, oldVal){
							for(var i = 0;i < vm.valueMap.length;i++){
								if(!isNaN(vm.value) && !isNaN(vm.valueMap[i]) && parseInt(vm.value) == parseInt(vm.valueMap[i])){
									var $index = i;
									break;
								}else if(typeof vm.value == "object" && typeof vm.valueMap[i] == "object" && JSON.stringify(vm.value) == JSON.stringify(vm.valueMap[i])){
									var $index = i;
									break;
								}else if(vm.value == vm.valueMap[i]){
									var $index = i;
									break;
								}
							}

							if(typeof $index == "undefined" && typeof vm.limit != "undefined"){
								var allCollection = vm.collectionFn();
								var valueMap = allCollection.map(vm.valueFn);
								var displayMap = allCollection.map(vm.displayFn);
								
								if(vm.limit < allCollection.length){
									for(var i = 0;i < valueMap.length;i++){
										if(!isNaN(vm.value) && !isNaN(valueMap[i]) && parseInt(vm.value) == parseInt(valueMap[i])){
											var $index = i;
											break;
										}else if(typeof vm.value == "object" && typeof valueMap[i] == "object" && JSON.stringify(vm.value) == JSON.stringify(valueMap[i])){
											var $index = i;
											break;
										}else if(vm.value == valueMap[i]){
											var $index = i;
											break;
										}
									}
								}
							}

							vm.selectedIndex = $index;
							vm.cursorIndex = $index;
							vm.text = typeof allCollection == "undefined" ? vm.displayMap[$index] : vm.displayFn(allCollection[$index]);

							if(ctrl){ ctrl.$setViewValue(vm.value); }
						});

						$scope.$watch(function(){
							for(var i = 0;i < vm.valueMap.length;i++){
								if(!isNaN(vm.value) && !isNaN(vm.valueMap[i]) && parseInt(vm.value) == parseInt(vm.valueMap[i])){
									return false;
								}else if(typeof vm.value == "object" && typeof vm.valueMap[i] == "object" && JSON.stringify(vm.value) == JSON.stringify(vm.valueMap[i])){
									return false;
								}else if(vm.value == vm.valueMap[i]){
									return false;
								}
							}

							if(typeof vm.value != "undefined" && typeof vm.limit != "undefined"){
								var allCollection = vm.collectionFn();
								var valueMap = allCollection.map(vm.valueFn);
								var displayMap = allCollection.map(vm.displayFn);
								
								if(vm.limit < allCollection.length){
									for(var i = 0;i < valueMap.length;i++){
										if(!isNaN(vm.value) && !isNaN(valueMap[i]) && parseInt(vm.value) == parseInt(valueMap[i])){
											return false;
										}else if(typeof vm.value == "object" && typeof valueMap[i] == "object" && JSON.stringify(vm.value) == JSON.stringify(valueMap[i])){
											return false;
										}else if(vm.value == valueMap[i]){
											return false;
										}
									}
								}
							}

							return true;
						}, function(newVal, oldVal){
							if ( newVal ){
								vm.selectedIndex = vm.cursorIndex = vm.firstIndex;
								vm.text = "";
								vm.value = undefined;
							}
						})
					}
				});
				
				//Limpieza
				$scope.$on("$destroy", function() {
					vm.searcher.destroy();
					$document.unbind('click', clickOut);
					dropdown.remove();
				});
			}	
		}
	})
})();

/**
**/
(function(){ 'use strict' //AC-DROPDOWN
	//Material dropdown
	angular.module('ngControls').directive("acDropdown", function($document, $compile, $timeout, PHI){
		var linkFn = function($scope, $element, $attrs, ctrl, $transclude){	
			var dropdownItems = $element[0].querySelectorAll(".ac-dropdown-item");

			var dropdown = angular.element(
				"<div class = 'ac-dropdown-list'>"+
				"</div>"
			);
			for ( var i = 0; i < dropdownItems.length; i++ ){
				dropdown[0].appendChild(dropdownItems[i]);
			}
			//dropdown = $compile(dropdown)($scope);

			
			var size = 15;
			if ("size" in $attrs) size = $attrs.size;
			/*
			dropdown.css({
				fontSize:size+"px"
			});
			*/
			
			//var aes = dropdown.find("a").css({padding:size / PHI + "px "+size+"px"});
			
			//El gestor de click en el documento (el que cierra el desplegble)
			var clickHandler = function(event){
				var isChild = $element[0].contains(event.target);
				var isSelf = $element[0] == event.target;

				if (!isChild && !isSelf) {
					dropdown.removeClass("active");
					dropdown.css("transform", "translateY(-"+size+"px)");	
					/*
					$timeout(function(){
						dropdown.hide();
					}, 300);
					*/
				}	
			}
			
			
			$document.bind('click', clickHandler);
			$element.on("click", function(){
				dropdown.css("transform", "translateY(-"+size+"px)");	
				$document.find("body").append(dropdown);
				var rect = $element[0].getBoundingClientRect();
				var width = rect.width; //Min width
				for ( var i = 0; i < dropdownItems.length; i++ ){
					var w = dropdownItems[i].getBoundingClientRect().right - dropdownItems[i].getBoundingClientRect().left;
					if ( w > width ) width = w;
				}
				
				var pos;
				if ( $attrs.position ) pos = $attrs.position;
				else{
					if ( window.innerWidth - rect.right > width ) pos = "left";
					else pos = "right";
				}	
				var top = rect.top;
				
				var height = rect.height;
				var left;
				if ( pos == "left" ) left = rect.left;	
				else  left = rect.left - width + rect.width;

				dropdown.css({
					left:left + "px",
					top:top + height + "px",
					width:width + "px",
					transform: "translateY(0px)"
				});
				dropdown.addClass("active");
				angular.forEach(dropdownItems, function(el){
					angular.element(el).css("width", "100%");
				});
			});

			$scope.$on("$destroy", function(){
				dropdown.remove();
				$document.unbind('click', clickHandler);
			});	
		}
		
		return {
			restrict:"C",
			replace:true,
			transclude:true,
			template:"<div><ng-transclude></ng-transclude></div>",
			link:linkFn
		}
	})

})();

/**
**/
(function(){ 'use strict' //AC-TABSET
	angular.module('ngControls').directive('acTabset', function($timeout) { //Tabs
		return {
			restrict: 'AC',
			transclude: true,
			scope: { 
				resize:"=" 
			},
			template: '  <ul class = "tablist">'+
				'	<li ng-repeat="tab in tabsetCtrl.tabs track by $index" ng-class="{\'active\': tab.active}">'+
				'	  <a class = "ac-ripple" ng-click="tabsetCtrl.select($index)">'+
				'		<span style = "font-size: 23px; color: #ff5252; position: absolute; left:13px" ng-show = "tab.$invalid">!</span>' +
				'		{{tab.label}}'+
				'	  </a>'+
				'	</li>'+
				'  </ul>'+
				'	<div class = "bar" ng-style="barStyle"></div>'+
				'   <div class = "wrapper" ng-transclude ng-style="wrapperStyle">'+
				'  </div>',
			bindToController: true,
			controllerAs: 'tabsetCtrl',
			controller: function($scope) {
				var self = this
				self.tabs = [];
				self.addTab = function addTab(tab) {
					self.tabs.push(tab)
					if(self.tabs.length === 1) {
						tab.active = true;
					}
				}
				self.select = function(selectedTabIndex) {
					angular.forEach(self.tabs, function(tab, i) {
						if(tab.active && i !== selectedTabIndex) {
							tab.active = false;
						}
					});
					self.tabs[selectedTabIndex].active = true;
					$scope.selectedTabIndex = selectedTabIndex;
				}
			},
			link:function($scope, $element, attrs, ctrl){
				var watchers = [];
				$timeout(function(){
					$scope.tabs = ctrl.tabs;
					$scope.selectedTabIndex = 0;
					$scope.barStyle = {};
					$scope.wrapperStyle = {};
					watchers = [
						$scope.$watch("selectedTabIndex", function(newVal, oldVal){
							var li = $element.find("li")[newVal];
							var rect = li.getBoundingClientRect();
							
							var orig = $element.find("li")[0].getBoundingClientRect().left;
							$scope.barStyle = { left:(rect.left - orig)+"px", width:rect.right - rect.left+"px"};

							if (ctrl.resize){
								var setAuto = false;
								if (!("height" in $scope.wrapperStyle)) setAuto = true;

								if (!setAuto){
									var height = $element[0].querySelector(".wrapper").getBoundingClientRect().height;
									$scope.wrapperStyle = {height:height+"px"};
									$timeout(function(){
										var newHeight = $element[0].querySelectorAll(".wrapper .ac-tab-item")[newVal].getBoundingClientRect().height;
										$scope.wrapperStyle = {height:newHeight+"px"};
										$timeout(function(){
											$scope.wrapperStyle = {height:"auto"};
										}, 300);
									}, 0);
								}
								else{
									$scope.wrapperStyle = {height:"auto"};
								}
							}
						})
					];
				},0)
				
				//Creamos un observer que elimine los dropdown en caso de que la directiva desaparezca
				var observer = new MutationObserver(function(mutations) {
					if (!document.body.contains($element[0])){
						observer.disconnect();
						for ( var i = 0; i < watchers.length; i++ ){
							watchers[i]();
						}
						$scope.$destroy();
						return;
					}
				});	 
				var config = { childList: true, subtree: false  /*attributes: true, characterData: true*/ };
				observer.observe(document.querySelector('body'), config);
			}
		}
	});
	angular.module('ngControls').directive('acTabItem', function($timeout) { //Tab item
		function acTabItemController($scope, $element){
			var controls = [];
			
			var self = this;
			
			self.$addControl = function(control){
				controls.push(control);
				control.$$parentTab = self;	
				
				if (control.$name) {
					self[control.$name] = control;
					control.$$parentTab = self;	
				}
			}
			self.$setValidity = function(validationErrorKey, state, control){
				$scope.$invalid = false;

				if ( state === false ) $scope.$invalid = true;
				else{
					for ( var i = 0; i < controls.length; i++ ){
						if ( controls[i].$invalid ){
							$scope.$invalid = true;
							break;
						}
					}
				}	
				$scope.$valid = !$scope.$invalid;
			}
			self.$removeControl = function(control){
				controls.splice(controls.indexOf(control), 1);
				if (control.$name) {
					delete self[control.$name];
					control.$$parentTab = undefined;	
				}
				
				if ( $scope.$invalid ){
					$scope.$invalid = false;
				
					for ( var i = 0; i < controls.length; i++ ){
						if ( controls[i].$invalid ){
							$scope.$invalid = true;
							break;
						}
					}
					
					$scope.$valid = !$scope.$invalid;
				}
			}
		}
	
		return {
			restrict: 'AC',
			transclude: true,
			replace:true,
			template: '<div ng-show="active" ng-transclude ng-style = "style"></div>',
			require: '^acTabset',
			scope: { label: '@' },
			controller: acTabItemController,
			controllerAs:"acTabItemController",
			link: function(scope, elem, attr, tabsetCtrl) {
				scope.active = false;
				tabsetCtrl.addTab(scope);
				scope.style = {};
				
				var watchers = [
					scope.$watch("active", function(newVal, oldVal){
						if ( newVal === oldVal ) return;
						
						if ( newVal ){
							$timeout(function(){
								scope.style = {overflow:"visible"};
							}, 0);
						}
						else{
							scope.style = {};
						}	
					})
				]
			}	
		}
	})
	//AÃƒÂ±adir los controles a los tab
	.config(function($provide) {
		$provide.decorator('ngModelDirective', function($delegate) {
			var directive = $delegate[0];
			var compile = directive.compile;

			directive.compile = function(){
				var cmArgs = arguments;
				var o = compile.apply(this, cmArgs);
				var postLink = o.post;
				
				o.post = function(scope, element, attr, ctrls){
					postLink.call(this, scope, element, attr, ctrls);			
					if ( ctrls[3] ){
						ctrls[3].$addControl(ctrls[0]);
						var setValidity = ctrls[0].$setValidity;
					
						
						ctrls[0].$setValidity = function(){
							var svArgs = arguments;
							setValidity.apply(this, svArgs);
							ctrls[0].$$parentTab.$setValidity(svArgs[0], svArgs[1], ctrls[0]);
							
							
						}
						element.on('$destroy', function() {
							ctrls[3].$removeControl(ctrls[0]);
						});
					}
				}		
				return o;
			}

			directive.require.push("^?acTabItem");
			return $delegate;
		});
	})
})();

/**
**/
(function(){ 'use strict' //AC-GRID
	angular.module('ngControls').directive('acGrid', function($timeout) { //Tabs
		return {
			restrict: 'EAC',
			link:function($scope, $element, $attrs){
				var acGridOptions = {};
				if ( "acGridOptions" in $attrs ){
					acGridOptions = $scope.$eval($attrs.acGridOptions);
				}
				if ( !("spacing" in acGridOptions) ) acGridOptions.spacing = 0;
				var grid = [
					/*[],[], [], ...*/
				];
				var childrens = $element.children();
				var currentLine = 0;
				for ( var i = 0; i < childrens.length; i++ ){
					if ( grid.length == currentLine ) grid[currentLine] = [];
					console.debug(childrens[i]);
					if ( childrens[i].tagName.toLowerCase() == "br" ) currentLine+=1;
					else grid[currentLine].push(childrens[i]);
				}
				
				for ( var i = 0; i < grid.length; i++ ){
					var hp = acGridOptions.spacing;
					var vp = acGridOptions.spacing;
					
					var p;
					if ( i == grid.length -1 ) p = 0;
					else p = vp;

					var elements = angular.element(grid[i]);
					$(elements).wrapAll("<div style = 'display: inline-block; padding-bottom:"+p+"px; width:100%;'></div>");

					var el = grid[i][0];
					var wrapper = $(el).parent();
					var width = wrapper.width();
					var w = width / grid[i].length;
					var d = hp * (grid[i].length -1) / grid[i].length;

					for ( var j = 0; j < grid[i].length; j++ ){
						if ( j == grid[i].length -1 ) p = 0;
						else p = hp;
						var element = angular.element(grid[i][j]);
						$(element).wrap("<div style = 'display: inline-block; box-sizing:content-box; padding-right:"+p+"px; width:"+(w-d)+"px;'></div>");
					}
					
					//Reajustamos los elementos
					var oe = {};
					var spaceLeft = width;
					var oel = 0;
					for ( var j = 0; j < grid[i].length; j++ ){
						var w = $(grid[i][j]).width();
						var parentWidth = $(grid[i][j]).parent().width();

						if ( w != parentWidth ){
							oe[j] = w;
							spaceLeft-= w;
							oel+=1;
						}
					}
					
					//Corregimos la delta para dividir entre los elementos restantes, ya que los reajustados usan el ancho que se les haya dado
					console.debug(d);
					d+= d*oel / (grid[i].length - oel);
					console.debug(d*oel / (grid[i].length - oel));
					console.debug(d);
					
					var newW = spaceLeft / (grid[i].length - oel);
					for ( var j = 0; j < grid[i].length; j++ ){
						if ( j in oe ) $(grid[i][j]).parent().css({
							width: oe[j] + "px"
						});
						else $(grid[i][j]).parent().css({
							width: (newW - d) + "px"
						});
					}
				}		
			}
		}
	});
})();

/**
**/
(function(){ 'use strict' //AC-ANIMATE
	angular.module('ngControls').directive('acAnimate', function($window, $interval, $timeout) { //Tabs
		function isGoingToBeVisible(el) {
			if (typeof jQuery === "function" && el instanceof jQuery) {
				el = el[0];
			}
			var rect     = el.getBoundingClientRect(),
				vWidth   = window.innerWidth || doc.documentElement.clientWidth,
				vHeight  = window.innerHeight || doc.documentElement.clientHeight,
				efp      = function (x, y) { return document.elementFromPoint(x, y) };     
			var topLine = window.innerHeight + document.body.scrollTop;
			return rect.top < topLine;
		}
	
		function findKeyframesRule(rule) {
			var ss = document.styleSheets;
			for (var i = 0; i < ss.length; ++i) {
				for (var j = 0; j < ss[i].cssRules.length; ++j) {
					if ( (ss[i].cssRules[j].type == window.CSSRule.WEBKIT_KEYFRAMES_RULE || ss[i].cssRules[j].type == window.CSSRule.KEYFRAMES_RULE ) && ss[i].cssRules[j].name == rule) { 
						return ss[i].cssRules[j]; 
					}
				}
			}
			return null;
		}
		
		return {
			restrict: 'A',
			link:function($scope, $element, $attrs){
				var transition = $element.attr("ac-animate");
				$element.css("display", "none");
				
				//Encontrar la regla de animaciÃƒÂ³n aplicada y ponemos el estilo correspondiente al estado inicial
				var rule = findKeyframesRule(transition);
				console.debug(rule);
				var style = rule.cssRules[0].style;
				var cssRules = [];
				for ( var i = 0; i < style.length; i++ ){
					var styleName = style[i];
					$element.css(styleName, style[styleName]);
				}

				//Aplicar opciones
				var options = {
					delay:0,
					animateWhen:"visible", /*immediate*/
					duration:300
				}
				var customOptions;
				try{
					customOptions = $scope.$eval($attrs.acAnimateOptions);
				}
				catch(err){
					customOptions = {};
				}
				for ( var i in customOptions ) options[i] = customOptions[i];
				
				//Creamos la funciÃƒÂ³n de animaciÃƒÂ³n
				var animate = function(){
					$element.css("animationDuration", options.duration+"ms");
					$element.css("animationName", transition);
					$element.css("animationFillMode", "forwards");
					$element.css("animationDelay", options.delay+"ms");
				}
				
				
				//
				var checkVisibility;
				var tick = $interval(function(){
					$element.css("display", "");
					if ( options.animateWhen == "visible" && isGoingToBeVisible($element) ){
						animate();
						$interval.cancel(tick);
					}
					else if ( options.animateWhen == "immediate" ){
						animate();
						$interval.cancel(tick);
					}
				}, 10);
			}
		}
	});
})();

/**
**/
(function(){ 'use strict' //AC-CAROUSEL
	angular.module('ngControls').directive('acCarousel', function($window, $interval, $timeout, $compile, $parse, PHI) {
		//Animaciones predefinidas
		var slideFromRight = function(from, to, timeout){
			from.css({transition: (timeout/1000) + "s ease-in-out all"});
			to.css({left:"100%", display:"", transition: (timeout/1000) + "s ease-in-out all"});
			
			$timeout(function(){
				from.css({left:"-100%"});
				to.css({left:"0%"});
				
				$timeout(function(){
					from.css({left:"",display:"none"});
				}, timeout);		
			}, 0);
		}
		var slideFromLeft = function(from, to, timeout){
			from.css({transition: (timeout/1000) + "s ease-in-out all"});
			to.css({left:"-100%", display:"", transition: (timeout/1000) + "s ease-in-out all"});
			
			$timeout(function(){
				from.css({left:"100%"});
				to.css({left:"0%"});
				
				$timeout(function(){
					from.css({left:"",display:"none"});
				}, timeout);		
			}, 0);
		}

		return {
			restrict: 'EAC',
			transclude: true,
			controller:function($scope, $element, $attrs, $transclude){
				var self = this;
				self.index = 0;
				self.bullets = [];
				self.config = {
					bullets:true,
					arrows:true,
					timeout:3000,
					height:$window.innerHeight/PHI,
					bulletSize:19,
					arrowSize:53,
					slidesToShow:1,
					slideWidth:$element.width()
				}
				self.slides = [];
				self.timer = null;
				
				self.changeIndex = function(newIndex, f){		
					if ( newIndex == self.index ) return;
					if ( newIndex < 0 ) newIndex = self.slides.length - 1;
					if ( newIndex > self.slides.length - 1 ) newIndex = 0;
					
					var oldIndex = self.index;
					self.index = newIndex;
					
					
					if ( self.config.slidesToShow > 1 ){	
						self.placeSlides();
					}
					else{
						if ( f ){
							f.call(self, angular.element(self.slides[oldIndex]), angular.element(self.slides[newIndex]), 300);
						}
						else{
							if ( newIndex > oldIndex) slideFromRight(angular.element(self.slides[oldIndex]), angular.element(self.slides[newIndex]), 300);
							else slideFromLeft(angular.element(self.slides[oldIndex]), angular.element(self.slides[newIndex]), 300);
						}
					}

					if ( self.config.bullets ){
						angular.element(self.bullets[oldIndex]).css({
							boxShadow:""
						})
						angular.element(self.bullets[newIndex]).css({
							boxShadow:"0px 0px 1px 2px white inset"
						})
					}
					
					if ( self.config.timeout ){
						$timeout.cancel(self.timer);
						self.timer = $timeout(self.nextSlide, self.config.timeout);
					}
				}
				self.nextSlide = function(){
					self.changeIndex(parseInt(self.index) + 1, slideFromRight);
				}
				self.prevSlide = function(){
					self.changeIndex(parseInt(self.index) - 1, slideFromLeft);
				}
				
				self.placeSlides = function(){
					var num_spaces = self.config.slidesToShow-1; 
					var space = $element.width() - self.config.slideWidth*self.config.slidesToShow;
					var spacing = space / num_spaces;
					
					for ( var i = 0; i < self.slides.length; i++ ){
						var slide = angular.element(self.slides[i]);
						
						var offsetLeft = (self.config.slideWidth + spacing)*( i - self.index );
						slide.css({left:offsetLeft+"px", width:self.config.slideWidth+"px"});
						//slide.css({transition: (300/1000) + "s ease-in-out all", left:offsetLeft+"px", width:self.config.slideWidth+"px"});
						
						
						if ( self.config.slidesToShow == 1 && i != self.index ) slide.css({display:"none"});
						else slide.css({display:""});
					}
					
					/*
					for ( var i = self.index; i < self.index + self.config.slidesToShow; i++ ){		
						angular.element(self.slides[i]).css({
							transition: (300/1000) + "s ease-in-out all",
							display:""
						});
					}
					var offsetLeft = (self.config.slideWidth+ spacing)*( i - self.index );	
					angular.element(self.slides[i]).css({
						left:offsetLeft+"px",
						width:self.config.slideWidth+"px"
					});
					
					$timeout(function(){
						for ( var i = self.index; i < self.index + self.config.slidesToShow; i++ ){
							var offsetLeft = (self.config.slideWidth+ spacing)*( i - self.index );	
							
							angular.element(self.slides[i]).css({
								left:offsetLeft+"px",
								width:self.config.slideWidth+"px"
							});
								
							angular.element(self.slides[i]).css({
								left:offsetLeft+"px",
								width:self.config.slideWidth+"px"
							});
						}
					}, 0);
					
					
					for ( var i = 0; i < self.slides.length; i++ ){	
						if ( i >= self.index + self.config.slidesToShow ){
							angular.element(self.slides[i]).css({
								display:"none"
							})
						}
					}
					*/
				}
				
				self.init = function(){
					//ConfiguraciÃƒÂ³n del slider
					var customConfig;
					try{
						customConfig = $scope.$eval($attrs.acCarouselOptions);
					}
					catch(err){
						customConfig = {};
					}
					for ( var i in customConfig ) self.config[i] = customConfig[i];
	
					self.slides = [];
					var elements = $transclude();
					for ( var i = 0; i < elements.length; i++ ){
						if ( elements[i].nodeType == 3 ) continue;
						angular.element(elements[i]).addClass("ac-carousel-slide");
						self.slides.push(elements[i]);
						$element.append(elements[i]);
					}
					$element.css({
						height:self.config.height+"px"
					});
					
					//Creamos un wrapper para los controles que compilaremos al final
					var controlsWrapper = angular.element("<div class = 'ac-carousel--control-wrapper'></div>");
					$element.append(controlsWrapper);
					
					//Crear los bullets
					if ( self.config.bullets ){
						var bulletWrapper = angular.element("<div class = 'ac-carousel--bullet-wrapper'></div>")
						controlsWrapper.append(bulletWrapper);
						var marginSize = self.config.bulletSize / (PHI*2);
						
						var wrapperSize = self.config.bulletSize*self.slides.length + marginSize*self.slides.length*2;
						var offsetLeft = ($element.width() - wrapperSize) / 2;
						bulletWrapper.css({
							width:wrapperSize+"px",
							left:offsetLeft+"px"
						})

						for ( var i = 0; i < self.slides.length; i++ ){
							var bullet = angular.element("<div class = 'ac-carousel--bullet ac-round-ripple' ripple-color = 'rgba(255, 255, 255, 0.7)'></div>");
							bullet.css({
								width:self.config.bulletSize + "px",
								height:self.config.bulletSize + "px",
								margin:marginSize+"px"
							});
							bullet.attr("ac-animate", "heart-beat");
							bullet.attr("ac-animate-options", "{delay:"+(i*100)+"}")
							bullet.attr("data-index", i);
							bulletWrapper.append(bullet);
							bullet.on("click", function(){
								var index = this.dataset.index;
								self.changeIndex(index);
							});	
							self.bullets.push(bullet);
						}
					}

					//Crear los arrows
					if ( self.config.arrows ){
						var marginSize = self.config.arrowSize / (PHI*2);
						var offsetTop = ( $element.height() - self.config.arrowSize*PHI ) / 2;
						
						var arrowLeft = angular.element("<div class = 'ac-carousel--arrow-left'></div>");
						arrowLeft.css({
							fontSize:self.config.arrowSize + "px",
							margin:marginSize+"px",
							width:self.config.arrowSize*(PHI/2) + "px",
							height:self.config.arrowSize*(PHI) + "px",
							top:offsetTop + "px"
						});
						var arrowRight = angular.element("<div class = 'ac-carousel--arrow-right'></div>");
						arrowRight.css({
							fontSize:self.config.arrowSize + "px",
							margin:marginSize+"px",
							width:self.config.arrowSize*(PHI/2) + "px",
							height:self.config.arrowSize*(PHI) + "px",
							top:offsetTop + "px"
						});
						
						controlsWrapper.append(arrowLeft);
						controlsWrapper.append(arrowRight);
						
						arrowLeft.on("click", self.prevSlide);
						arrowRight.on("click", self.nextSlide);
					}
					
					//Resto de inicializaciones
					if ( self.config.bullets ){
						angular.element(self.bullets[self.index]).css({
							boxShadow:"0px 0px 1px 2px white inset"
						});
					}

					//Colocamos los slides donde corrsponda
					self.placeSlides();
	
					$compile(controlsWrapper)($scope);
					if ( self.config.timeout ) self.timer = $timeout(self.nextSlide, self.config.timeout);
					
					$timeout(function(){
						for ( var i = 0; i < self.slides.length; i++ ){
							var slide = angular.element(self.slides[i]);
							slide.css({transition: (300/1000) + "s ease-in-out all"});
						}
					}, 0);
				}
			},
			link:function($scope, $element, $attrs, $ctrl, $transclude){
				if ("acCarouselName" in $attrs){
					var getter = $parse($attrs["acCarouselName"]);
					var setter = getter.assign;
					setter($scope, $ctrl);
					
					//$scope[$attrs["acCarouselName"]] = $ctrl;
				}
				$ctrl.init();
			}
		}
	});
})();



(function ngControls(){ 'use strict';
	//Devuelve una promesa que se resuelve con el desplegable de un select debidamente compilado, para que el componente acSelect lo enganche al body una vez resuelto
	var wrapAcSelectDropdown = function(scope, $templateRequest, $compile, $q){
		
	}
	//Enganchar el toggler a los checbox
	var wrapAcCheckboxToggler = function($element, $scope, $compile){
		var toggler = "<span class = 'ac-toggler' ng-class = '{toggled:value == trueValue}'></span>";
		$element.prepend($compile(toggler)($scope));
	}
	
	
	//Sacar el nombre de una variable dada una expresiÃƒÂ³n; por ej. dado empleado.nombre + " " + empleado.apellidos
	var getBaseExpr = function(expr, varName){
		var regex = new RegExp(varName+".","g");
		return expr.replace(regex, "");
	}
})();