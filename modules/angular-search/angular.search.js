(function ngSearch(){ 'use strict';
	angular.module('ngSearch', [])
		.factory('StringService', function(){
			var Latinise={};
			Latinise.latin_map={"Á":"A","Â":"A","Ä":"A","À":"A","Å":"A","Ã":"A","Æ":"AE","Ç":"C","Ð":"D","É":"E","Ê":"E","Ë":"E","È":"E","ƒ":"F","Í":"I","Î":"I","Ï":"I","Ì":"I","Ñ":"N","Ó":"O","Ô":"O","Ö":"O","Ò":"O","Ø":"O","Õ":"O","Š":"S","Ú":"U","Û":"U","Ü":"U","Ù":"U","Ý":"Y","Ÿ":"Y","Ž":"Z","Œ":"OE","á":"a","â":"a","ä":"a","à":"a","å":"a","ã":"a","æ":"ae","ç":"c","é":"e","ê":"e","ë":"e","è":"e","í":"i","î":"i","ï":"i","ì":"i","ñ":"n","ó":"o","ô":"o","ö":"o","ò":"o","ø":"o","õ":"o","š":"s","ú":"u","û":"u","ü":"u","ù":"u","ý":"y","ÿ":"y","ž":"z","œ":"oe"};

			return {
				parse_str:function(str){
					var arr = str.split("&");
					var obj = {};
					
					for ( var i = 0; i < arr.length; i++ ){
						var arr2 = arr[i].split("=");
						obj[arr2[0]] = arr2[1];
					}
					
					return obj;
				},
				removeAccents:function(string){return string.replace(/[^A-Za-z0-9\[\] ]/g,function(a){return Latinise.latin_map[a]||a})},
				isLatin:function(string){ return this.removeAccents(string) == string; },
				endsWith:function(string, termination, position){
					var subjectString = string.toString();
					if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
						position = subjectString.length;
					}
					position -= termination.length;
					var lastIndex = subjectString.indexOf(termination, position);
					return lastIndex !== -1 && lastIndex === position;
				},
				replaceTermination:function(string, $find, $replace){
					var $regexp = new RegExp($find+"$");
					return string.replace($regexp, $replace);
				},
				extractKeywords:function(string) {
					var $string = new String(string);
					$string = this.removeConnectors(this.removeAccents($string));
					$string = $string.replace(/[^A-Za-z0-9_]/g, '-');
					var $keywords = $string.split("-");
					for ( var i = 0; i< $keywords.length; i++ ) { 
						$keywords[i] = this.removePlurals($keywords[i]).toLowerCase();	
					}
					return $keywords;
				},
				removePlurals:function(string){
					var $string = string;
					
					if ( this.endsWith(string, "ones") ) $string = this.replaceTermination(string, "ones", "ón"); 
					else if ( this.endsWith(string, "tes") ) $string = this.replaceTermination(string, "tes", "te");
					else if ( this.endsWith(string, "es") ) $string = this.replaceTermination(string, "es", "");
					else if ( this.endsWith(string, "as") ) { $string = this.replaceTermination(string, "as", "a"); }
					else if ( this.endsWith(string, "s") && !this.endsWith(string, "us") ) $string = this.replaceTermination(string, "s", "");
					return $string;
				},
				removeConnectors:function(string) { 
					var $conectores = ["y", "de", "a", "del", "el", "la", "los", "las", "en", "entre", "desde", "hasta"];
					var $regexp = /\s/g; 
					var $words = string.split($regexp);

					var $valid_words =[];
					for ( var i in $words ){
						if ( $conectores.indexOf($words[i]) == -1 && $words[i].length > 2){
							$valid_words.push($words[i]);
						}
					}
					return $valid_words.join(" ");
				},
				levenshtein:function(a, b) {
					if(a.length == 0) return b.length; 
					if(b.length == 0) return a.length;

					// swap to save some memory O(min(a,b)) instead of O(a)
					if(a.length > b.length) {
						var tmp = a;
						a = b;
						b = tmp;
					}

					var row = [];
					// init the row
					for(var i = 0; i <= a.length; i++){
						row[i] = i;
					}

					// fill in the rest
					for( var i = 1; i <= b.length; i++){
						var prev = i;
						for(var j = 1; j <= a.length; j++){
							var val;
							if(b.charAt(i-1) == a.charAt(j-1)){
								val = row[j-1]; // match
							} else {
								val = Math.min(row[j-1] + 1, prev + 1, row[j] + 1);
							}
							row[j - 1] = prev;
							prev = val;
						}
						row[a.length] = prev;
					}

					return row[a.length];
				}
			}
		})
		.factory('ngSearch', function($q, $timeout, $interval, $parse, $rootScope, StringService){
			//Esta función rellena las opciones del buscador con valores predeterminados si no son provistos en la entrada de la función.
			function buildOptions(options){
				options = options || {};
				options.page = options.page || 1;
				options.limit = options.limit || 5000;
				options.searchIn = options.searchIn || "*";
				if ( options.searchIn instanceof Array ) options.searchIn = options.searchIn.join(",");
				options.chunkSize = options.chunkSize || 100;
				options.sort = options.sort || undefined;
				options.debounce = options.debounce || 0;
			}
			
			//Esta función va construyendo un mapa de pesos en función de la búsqueda realizada
			function buildWeightMap(collection, wordsSearched, $index, weights, options){
				for ( var k = 0; k < options.chunkSize; k++ ){
					if ( $index >= collection.length ) break;

					var item = collection[$index];
					var weight = 0;	
					for ( var i in wordsSearched ){
						var word = StringService.removeAccents(wordsSearched[i]).toLowerCase();
						if ( word.length < 3 ) continue;
						var distance = 100;

						if ( typeof options.searchIn == "string" ){
							if ( options.searchIn == "*" ){
								for ( var key in item ){
									if ( !item[key] ) continue;
									var keywords = StringService.extractKeywords(item[key].toString());
									for ( var j in keywords ){
										if ( !keywords[j] ) continue;	
										
										var keyword = StringService.removeAccents(keywords[j].toString()).toLowerCase();
										
										distance = Math.min( StringService.levenshtein(keyword, word), distance );

										if ( keyword.indexOf(word) !== -1 ){
											distance = Math.min(distance, 0.5);
										}
									}
								}
							}
							else{
								var $searchInArray = options.searchIn.split(",").map(function(a){ return a.trim(); });
								for ( var ii = 0; ii < $searchInArray.length; ii++ ){
									var textoIdentificativo = $parse($searchInArray[ii])(item);
									var keywords = StringService.extractKeywords(textoIdentificativo);
									for ( var j in keywords ){
										if ( !keywords[j] ) continue;	
										
										var keyword = StringService.removeAccents(keywords[j].toString()).toLowerCase();
										
										distance = Math.min( StringService.levenshtein(keyword, word), distance );

										if ( keyword.indexOf(word) !== -1 ){
											distance = Math.min(distance, 0.5);
										}
									}
								}
							}
						}

						weight+=100/Math.max(1,distance*1.618);
					}
					weights.push(weight);
					$index+=1;
				}
				
				return $index;
			}
			
			//Una vez que tenemos el mapa de pesos, resolvemos la búsqueda y devolvemos la conexión
			function sortByMatches(collection, weights){
				var weightsMap = [];
				var maxWeight = Math.max.apply(Math, weights);

				var filteredCollection = collection.filter(function(item, index){
					var weight = weights[index];
					if ( weight*1.618 >= maxWeight ){
						weightsMap.push(weight);
						return true;
					}
					//if ( weight > 1 ) return true;
					else return false;
				});
				
				var weightsMap = weightsMap.map(function(w, index){
					return {
						weight:w,
						index:index
					}
				});

				weightsMap.sort(function(a, b){
					if (a.weight > b.weight) return -1;
					else if (b.weight > a.weight) return 1; 
					else return 0;
				});

				var sortedCollection = [];
				for ( var i = 0; i < weightsMap.length; i++ ){
					var index = weightsMap[i].index;
					sortedCollection.push(filteredCollection[index]);
				}
				
				return sortedCollection;
			}

			//Ordenar por campos
			function sortByFields(collection, sortBy){
				var $sortArray = sortBy.split(",").map(function(a){ return a.trim(); });
				
				if(typeof collection == "undefined") return [];

				return collection.slice().sort(function(a, b){		
					for ( var i = 0; i < $sortArray.length; i++ ){
						var sort = $sortArray[i];
						var lc, aVal, bVal;
						
						if ( sort.charAt(0) == "-" ){
							aVal = $parse(sort.substring(1))(a);
							bVal = $parse(sort.substring(1))(b);
						}
						else{
							aVal = $parse(sort)(a);
							bVal = $parse(sort)(b);
						}

						if(!isNaN(aVal) && !isNaN(bVal)){
							aVal = parseFloat(aVal);
							bVal = parseFloat(bVal);

							if ( sort.charAt(0) == "-" ) lc = bVal < aVal ? -1 : (bVal > aVal ? 1 : 0);
							else lc = aVal < bVal ? -1 : (bVal < aVal ? 1 : 0);
						}else{
							aVal = aVal === undefined ? "0" : aVal === null ? "0" : aVal === false ? "0" : aVal === true ? "1": aVal.toString();
							bVal = bVal === undefined ? "0" : bVal === null ? "0" : bVal === false ? "0" : bVal === true ? "1": bVal.toString();
							
							if ( sort.charAt(0) == "-" ) lc = bVal.localeCompare(aVal);
							else lc =  aVal.localeCompare(bVal);
						}

						if ( lc != 0 ) return lc;
					}
					return 0;	
				});
			}
			
			//El buscador
			function Searcher(options){
				var self = this;
				self.isSearching = false;
				self.currentSearch = undefined;
				self.options = options || {};
				buildOptions(self.options);
				self.collection = [];
				self.total = undefined;
				self.first = undefined;
				self.last = undefined;
				self.page = self.options.page;
				self.limit = self.options.limit;
				self.orderBy = self.options.orderBy;
				self.filteredCollection = [];
				self.text = "";
				self.results = [];
				self.filter = self.options.filter;
				self.timestamp = undefined;
				self.debounce = self.options.debounce;
				
				self.setSource = function(collection){
					self.collection = collection;
					self.search();
				}

				
				self.listeners = {
					search:[], //Cuando comienza la búsqueda
					resolve:[] //Cuando se resuelve una búsqueda
				}
				self.on = function(action, callback){
					switch (action){
						case "search" : self.listeners.search.push(callback); break;
						case "resolve" : self.listeners.resolve.push(callback); break;
					}
				}
				
				self.resolve = function(){
					self.total = self.filteredCollection.length;
					self.first = Math.min((self.page-1)*self.limit+1, self.total);
					self.last = Math.min(self.total, self.limit*self.page);
					self.n_pages = Math.round(self.total/self.limit);
					self.results = self.filteredCollection.slice(self.first-1,self.last);
					self.currentSearch.resolve(self.results);
					self.isSearching = false;

					for ( var i = 0; i < self.listeners.resolve.length; i++ ) self.listeners.resolve[i].call(this, self.results);
				}
				
				self.sort = function(){
					self.filteredCollection = sortByFields(self.filteredCollection, self.orderBy);
					self.resolve();
				}

				
				var busquedas = [];
				
				self.search = function(){
					self.isSearching = true;
					for ( var i = 0; i < self.listeners.search.length; i++ ) self.listeners.search[i].call(this, self.text);
					self.currentSearch = $q.defer();
							
					var busqueda = {
						text:self.text,
						timestamp:(new Date).getTime(),
						fn:function(){
							var thisSearch = this;
							
							if ( thisSearch.text == "" ){
								if (typeof self.filter == "function" && self.collection) self.filteredCollection = self.collection.filter(self.filter);
								else self.filteredCollection = self.collection;
								if (self.orderBy){
									self.sort();
								}
								else{
									self.resolve();	
								}
							}
							else{
								var $index = 0;
								var wordsSearched = StringService.extractKeywords(self.text);
								var weightsMap = [];

								thisSearch.tick = function(){
									if ( thisSearch.cancelled ) return;
									
									if ( $index >= self.collection.length ){
										var filteredCollection = sortByMatches(self.collection, weightsMap);
											
										if (typeof self.filter == "function") self.filteredCollection = filteredCollection.filter(self.filter);
										else self.filteredCollection = filteredCollection;	
											
										self.resolve();	
									}
									else{
										$index = buildWeightMap(self.collection, wordsSearched, $index, weightsMap, self.options);
										$timeout(function(){
											thisSearch.tick();
										},1);
									}
								}
									
								$timeout(function(){
									thisSearch.tick();
								}, 0);
							}
						},
						cancel:function(){
							this.cancelled = true;
						}
					}

					if ( busquedas.length > 0 ){
						busquedas[0].cancel();
						busquedas = [];
					}
					busquedas.push(busqueda);
					busqueda.fn();
					
					return self.currentSearch.promise;
				}
			
				self.search();
				
				self.toggleOrder = function(field){
					var descString = "-"+field;
					var ascString = field;

					var $sortArray = self.orderBy.split(",").map(function(a){ return a.trim(); }).sort(function(a, b){
						if ( a == descString || a == ascString ) return -1;
						else if ( b == descString || b == ascString ) return 1;
						else return 0;
					});
					var orderBy = $sortArray.join(",");
					
					if ( orderBy.indexOf(descString) !== -1 ) self.orderBy = orderBy.replace(descString, ascString);
					else self.orderBy = orderBy.replace(ascString, descString); 
					
					console.debug(self.orderBy);
				}
				
				self.sortDirection = function(field){
					if (self.orderBy.indexOf("-"+field) !== -1 ) return "desc";
					else return "asc";
				}
				
				var debounceTimeout;
				
				var watchers = [
					$rootScope.$watch(function(){
						return self.page;
					}, function(newVal, oldVal){
						if ( newVal !== oldVal ) self.resolve();
					}),
					$rootScope.$watch(function(){
						return self.limit;
					}, function(newVal, oldVal){
						if ( newVal !== oldVal ) self.resolve();
					}),
					$rootScope.$watch(function(){
						return self.text;
					}, function(newVal, oldVal){
						if ( newVal !== oldVal) {
							self.page = 1;
							self.search();
						}	
					}),
					$rootScope.$watch(function(){
						return self.orderBy;
					}, function(newVal, oldVal){
						if ( newVal !== oldVal ) {
							self.page = 1;
							self.sort();
						}
					}),
					$rootScope.$watch(function(){
						return (self.page - 1)*self.limit > self.total;
					}, function(newVal, oldVal){
						if ( newVal === true ) self.page = 1;
					})
				]
				
				self.destroy = function(){
					for ( var i = 0; i < watchers.length; i++ ){
						watchers[i]();
					}
				};
				
				switch(typeof options.src){
					case "function":{
						watchers.push($rootScope.$watchCollection(function(){ 
							return options.src(); 
						}, function(newVal, oldVal){
							self.setSource(newVal);
						}));
					}
					break;
					
					default:{
						watchers.push($rootScope.$watchCollection(function(){ return options.src; }, function(newVal, oldVal){
							self.setSource(newVal);
						}));
					}
				}
				
				switch(typeof options.filter){
					case "function":{
						watchers.push($rootScope.$watchCollection(function(){
							if(self.collection) return self.collection.filter(self.filter);
							else true; 
						}, function(newVal, oldVal){
							//self.page = 1;
							self.search();
						}));
					}
					break;
					
					default:{
						//Otros tipos de filtros iran aquí, cuando toque.
					}
				}
				
				
				
				
				
				//self.filteredCollection = self.filteredCollection.filter(self.filter);
			}

			return {
				searcher: Searcher,
				get:function(options){
					var searcher = new Searcher(options);
					return searcher;
				}
			}
		})
})()